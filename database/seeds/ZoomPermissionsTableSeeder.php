<?php

namespace App\Seeders;

use App\CcpsCore\Permission;
use Uncgits\Ccps\Exceptions\InvalidSeedDataException;

use Uncgits\Ccps\Seeders\CcpsValidatedSeeder;

class ZoomPermissionsTableSeeder extends CcpsValidatedSeeder
{
    public $permissions = [
        // fill in permissions here
        [
           "name"         => "api.view",
           "display_name" => "API - View",
           "description"  => "View data from the live Zoom API",
        ],
        [
           "name"         => "archives.view",
           "display_name" => "Archives - View",
           "description"  => "View archived data from Zoom",
        ],
        [
           "name"         => "metrics.view",
           "display_name" => "Metrics - View",
           "description"  => "View Metrics, Metrics Snapshots, and Metrics Backfills",
        ],
        [
           "name"         => "metrics.create",
           "display_name" => "Metrics - Create",
           "description"  => "Create Metrics Snapshots and Metrics Backfills",
        ],
        [
           "name"         => "metrics.delete",
           "display_name" => "Metrics - Delete",
           "description"  => "Delete Metrics Snapshots and Metrics Backfills",
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $writeConsoleOutput = \App::runningInConsole();

        if ($writeConsoleOutput) {
            // get console output
            $output = $this->command->getOutput();
        }

        // validate
        try {
            $this->validateSeedData($this->permissions, $this->permissionArrayConstruction);
            $this->checkForExistingSeedData($this->permissions, Permission::all());

            $mergeData = [
                'source_package' => 'app',
                'created_at'     => date("Y-m-d H:i:s", time()),
                'updated_at'     => date("Y-m-d H:i:s", time()),
                'editable'       => 1
            ];

            $this->commitSeedData($this->permissions, 'ccps_permissions', $mergeData);
        } catch (InvalidSeedDataException $e) {
            if ($writeConsoleOutput) {
                $output->error($e->getMessage());
                return;
            }
        }
    }
}
