<?php

namespace App\Seeders;

use App\CcpsCore\Role;
use Uncgits\Ccps\Exceptions\InvalidSeedDataException;

use Uncgits\Ccps\Seeders\CcpsValidatedSeeder;

class ZoomRolesTableSeeder extends CcpsValidatedSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $writeConsoleOutput = \App::runningInConsole();

        if ($writeConsoleOutput) {
            // get console output
            $output = $this->command->getOutput();
        }

        $roles = [
            // fill in roles here
            [
               "name"         => "zoom.admin",
               "display_name" => "Zoom - administrator",
               "description"  => "UNCG Zoom administrators"
            ],
            [
               "name"         => "zoom.support",
               "display_name" => "Zoom - support",
               "description"  => "UNCG Zoom support staff"
            ],
        ];

        // validate
        try {
            $this->validateSeedData($roles, $this->roleArrayConstruction);
            $this->checkForExistingSeedData($roles, Role::all());

            $mergeData = [
                'source_package' => 'app',
                'created_at'     => date("Y-m-d H:i:s", time()),
                'updated_at'     => date("Y-m-d H:i:s", time()),
                'editable'       => 1
            ];

            $this->commitSeedData($roles, 'ccps_roles', $mergeData);
        } catch (InvalidSeedDataException $e) {
            if ($writeConsoleOutput) {
                $output->error($e->getMessage());
                return;
            }
        }
    }
}
