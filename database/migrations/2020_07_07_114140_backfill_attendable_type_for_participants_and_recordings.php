<?php

use App\ArchivedMeeting;
use App\ArchivedParticipant;
use App\ArchivedRecording;
use App\ArchivedWebinar;
use Illuminate\Database\Migrations\Migration;

class BackfillAttendableTypeForParticipantsAndRecordings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $meetingIds = ArchivedMeeting::pluck('uuid');
        $webinarIds = ArchivedWebinar::pluck('uuid');

        ArchivedParticipant::chunk(1000, function ($participant) use ($meetingIds, $webinarIds) {
            foreach ($participant as $p) {
                if ($meetingIds->contains($p->attendable_id)) {
                    $p->attendable_type = ArchivedMeeting::class;
                } elseif ($webinarIds->contains($p->attendable_id)) {
                    $p->attendable_type = ArchivedWebinar::class;
                }

                if ($p->isDirty('attendable_type')) {
                    $p->save();
                }
            }
        });

        ArchivedRecording::chunk(1000, function ($recording) use ($meetingIds, $webinarIds) {
            foreach ($recording as $r) {
                if ($meetingIds->contains($r->attendable_id)) {
                    $r->attendable_type = ArchivedMeeting::class;
                } elseif ($webinarIds->contains($r->attendable_id)) {
                    $r->attendable_type = ArchivedWebinar::class;
                }

                if ($r->isDirty('attendable_type')) {
                    $r->save();
                }
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
