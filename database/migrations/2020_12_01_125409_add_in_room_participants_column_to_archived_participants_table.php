<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddInRoomParticipantsColumnToArchivedParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('archived_participants', function (Blueprint $table) {
            $table->unsignedSmallInteger('in_room_participants')->after('version');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('archived_participants', function (Blueprint $table) {
            $table->dropColumn('in_room_participants');
        });
    }
}
