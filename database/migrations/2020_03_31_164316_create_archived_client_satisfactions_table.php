<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArchivedClientSatisfactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('archived_client_satisfactions', function (Blueprint $table) {
            $table->id();
            $table->string('date');
            $table->unsignedTinyInteger('satisfaction_percent');
            $table->unsignedInteger('good_count');
            $table->unsignedInteger('not_good_count');
            $table->unsignedInteger('none_count');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('archived_client_satisfactions');
    }
}
