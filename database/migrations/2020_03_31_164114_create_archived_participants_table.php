<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArchivedParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('archived_participants', function (Blueprint $table) {
            $table->id();
            $table->string('user_id');
            $table->string('zoom_id')->nullable();
            $table->string('meeting_id');
            $table->string('user_name');
            $table->string('device');
            $table->string('ip_address');
            $table->string('location');
            $table->string('network_type');
            $table->string('microphone')->nullable();
            $table->string('speaker')->nullable();
            $table->string('camera')->nullable();
            $table->string('data_center');
            $table->string('connection_type')->nullable();
            $table->timestamp('join_time')->nullable();
            $table->timestamp('leave_time')->nullable();
            $table->boolean('share_application');
            $table->boolean('share_desktop');
            $table->boolean('share_whiteboard');
            $table->boolean('recording');
            $table->string('pc_name')->nullable();
            $table->string('domain')->nullable();
            $table->string('mac_addr');
            $table->string('harddisk_id');
            $table->string('version');
            $table->string('leave_reason');
            $table->timestamps();

            $table->unique(['user_id', 'meeting_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('archived_participants');
    }
}
