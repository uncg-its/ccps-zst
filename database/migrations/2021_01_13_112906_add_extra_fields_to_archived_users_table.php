<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddExtraFieldsToArchivedUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('archived_users', function (Blueprint $table) {
            $table->string('role_id')->after('dept')->nullable();
            $table->string('host_key')->after('verified')->nullable();
            $table->string('plan_united_type')->after('host_key')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('archived_users', function (Blueprint $table) {
            $table->dropColumn('role_id');
            $table->dropColumn('host_key');
            $table->dropColumn('plan_united_type');
        });
    }
}
