<?php

use App\CcpsCore\CronjobMeta;
use Illuminate\Database\Migrations\Migration;

class AddArchiveReportsCronjobMeta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::transaction(function () {
            CronjobMeta::create([
                'class'  => 'App\Cronjobs\ArchiveReports',
                'status' => 'disabled'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $meta = CronjobMeta::where('class', 'App\Cronjobs\ArchiveReports')->delete();
    }
}
