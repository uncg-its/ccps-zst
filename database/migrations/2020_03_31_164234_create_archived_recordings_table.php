<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArchivedRecordingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('archived_recordings', function (Blueprint $table) {
            $table->id();
            $table->string('meeting_id');
            $table->string('zoom_id')->nullable();
            $table->timestamp('recording_start')->nullable();
            $table->timestamp('recording_end')->nullable();
            $table->string('file_type');
            $table->string('file_size')->nullable();
            $table->text('play_url')->nullable();
            $table->text('download_url')->nullable();
            $table->string('status')->nullable();
            $table->string('recording_type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('archived_recordings');
    }
}
