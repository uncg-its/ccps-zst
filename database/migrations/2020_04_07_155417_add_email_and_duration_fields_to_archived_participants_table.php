<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEmailAndDurationFieldsToArchivedParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('archived_participants', function (Blueprint $table) {
            $table->string('user_email')->nullable()->after('version');
            $table->unsignedInteger('duration')->nullable()->after('user_email');
            $table->timestamp('report_fetched_at')->nullable()->after('duration');
            $table->text('report_fetched_message')->nullable()->after('report_fetched_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('archived_participants', function (Blueprint $table) {
            $table->dropColumn('user_email');
            $table->dropColumn('duration');
            $table->dropColumn('report_fetched_at');
            $table->dropColumn('report_fetched_message');
        });
    }
}
