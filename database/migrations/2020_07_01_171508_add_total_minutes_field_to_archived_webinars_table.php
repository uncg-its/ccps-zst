<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTotalMinutesFieldToArchivedWebinarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('archived_webinars', function (Blueprint $table) {
            $table->unsignedInteger('total_minutes')->nullable()->after('dept');
            $table->timestamp('report_fetched_at')->nullable()->after('total_minutes');
            $table->text('report_fetched_message')->nullable()->after('report_fetched_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('archived_webinars', function (Blueprint $table) {
            $table->dropColumn('total_minutes');
            $table->dropColumn('report_fetched_at');
            $table->dropColumn('report_fetched_message');
        });
    }
}
