<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArchivedUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('archived_users', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email');
            $table->tinyInteger('type')->unsigned();
            $table->string('status');
            $table->string('pmi');
            $table->string('timezone')->nullable();
            $table->string('dept')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('last_login_time')->nullable();
            $table->string('last_client_version')->nullable();
            $table->json('group_ids')->nullable();
            $table->json('im_group_ids')->nullable();
            $table->text('pic_url')->nullable();
            $table->string('language')->nullable();
            $table->string('phone_number')->nullable();
            $table->boolean('verified');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('archived_users');
    }
}
