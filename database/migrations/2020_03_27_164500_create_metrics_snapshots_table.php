<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMetricsSnapshotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metrics_snapshots', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('date');
            $table->json('raw_data');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('pushed_at')->nullable();
            $table->string('push_destination')->nullable();
            $table->text('error_data')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('metrics_snapshots');
    }
}
