<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMetricPushesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metric_pushes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('metrics_snapshot_id');
            $table->string('pusher_class');
            $table->string('status');
            $table->json('messages')->nullable();
            $table->timestamp('pushed_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('metric_pushes');
    }
}
