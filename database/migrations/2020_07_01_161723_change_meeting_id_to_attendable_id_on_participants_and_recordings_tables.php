<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeMeetingIdToAttendableIdOnParticipantsAndRecordingsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('archived_participants', function (Blueprint $table) {
            $table->string('attendable_type')->nullable()->after('meeting_id');
            $table->renameColumn('meeting_id', 'attendable_id');
            $table->dropUnique(['user_id', 'meeting_id']);
            $table->unique(['user_id', 'attendable_id']);
        });

        Schema::table('archived_recordings', function (Blueprint $table) {
            $table->string('attendable_type')->nullable()->after('meeting_id');
            $table->renameColumn('meeting_id', 'attendable_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('archived_participants', function (Blueprint $table) {
            $table->renameColumn('attendable_id', 'meeting_id');
            $table->dropColumn('attendable_type');
            $table->dropUnique(['user_id', 'attendable_id']);
            $table->unique(['user_id', 'meeting_id']);
        });

        Schema::table('archived_recordings', function (Blueprint $table) {
            $table->renameColumn('attendable_id', 'meeting_id');
            $table->dropColumn('attendable_type');
        });
    }
}
