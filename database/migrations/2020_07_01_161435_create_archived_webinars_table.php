<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArchivedWebinarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('archived_webinars', function (Blueprint $table) {
            $table->string('uuid')->primary();
            $table->string('id');
            $table->string('topic');
            $table->string('host');
            $table->string('email');
            $table->string('user_type');
            $table->timestamp('start_time')->nullable();
            $table->timestamp('end_time')->nullable();
            $table->string('duration');
            $table->unsignedBigInteger('participants');
            $table->boolean('has_pstn');
            $table->boolean('has_voip');
            $table->boolean('has_3rd_party_audio');
            $table->boolean('has_video');
            $table->boolean('has_screen_share');
            $table->boolean('has_recording');
            $table->boolean('has_sip');
            $table->string('dept')->default('');
            $table->timestamp('participants_fetched_at')->nullable();
            $table->text('participants_fetched_message')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('archived_webinars');
    }
}
