<?php

return [
    // must be an eloquent model that implements 'Uncgits\Ccps\MetricsSnapshots\Contracts\Pushable'
    'metrics_model' => App\MetricsSnapshot::class,

    // must implement 'Uncgits\Ccps\MetricsSnapshots\Contracts\MetricPusher'
    'pusher_class'  => Uncgits\Ccps\MetricsClearingHousePusher\MchPusher::class,

    'log_channel'   => 'stack',
    'api_key'       => env('MCH_PUSHER_API_KEY'),
    'endpoint'      => env('MCH_PUSHER_API_ENDPOINT'),

    // do not push any snapshots past "now minus X" days. affords a built-in "safety period"
    'pad_days'      => 0,

    // required keys
    'required_keys' => [
        'service_name',
        'metric_name',
        'metric_value',
        'metric_timestamp',
    ],

    // key map
    'key_map' => [
        'service_name'     => 'service',
        'metric_name'      => 'name',
        'metric_value'     => 'value',
        'metric_dimension' => 'dimension',
        'metric_timestamp' => 'timestamp',
    ],

    // timestamp format
    'timestamp_format' => 'Y-m-d\TH:i:sO'
];
