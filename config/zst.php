<?php

return [
    'metrics' => [
        'service_name'                        => env('METRICS_SERVICE_NAME', env('APP_NAME')),
        'push_destinations'                   => explode(',', env('METRICS_PUSH_DESTINATIONS', 'log')),
        'days_ago'                            => env('METRICS_DAYS_AGO', 1),
        'participant_batch_size'              => env('METRICS_PARTICIPANT_BATCH_SIZE', 10),
        'participant_batch_timeframe_minutes' => env('METRICS_PARTICIPANT_BATCH_TIMEFRAME_MINUTES', 2),
        'meeting_length_thresholds'           => explode(',', env('METRICS_MEETING_LENGTH_THRESHOLDS', '')),

        'mch' => [
            'api_key' => env('METRICS_MCH_API_KEY'),
            'endpoint' => env('METRICS_MCH_ENDPOINT')
        ],
    ],

    'api' => [
        'rate_limit' => [
            'backoff_seconds' => explode(',', env('ZST_RATE_LIMIT_BACKOFF_SECONDS', '30,60,120'))
        ]
        ],

    'domain' => env('ZST_ZOOM_DOMAIN', 'uncg.edu'),
];
