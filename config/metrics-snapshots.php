<?php
namespace App;

return [
    // must be an Eloquent Model
    'snapshot_model_class' => \App\MetricsSnapshot::class,

    // must implement Uncgits\Ccps\MetricsSnapshots\MetricsSnapshotGenerator
    'generator_class' => \App\Services\MetricsSnapshotService::class,

    // must implement Uncgits\Ccps\MetricsSnapshots\Contracts\MetricsSnapshotRepositoryInterface
    'repository_class' => \Uncgits\Ccps\MetricsSnapshots\Repositories\MetricsSnapshotRepository::class,

    // must implement 'Uncgits\Ccps\MetricsSnapshots\Contracts\MetricPusher'
    'default_pushers' => [
        \Uncgits\Ccps\MetricsClearingHousePusher\MchPusher::class,
    ],

    // for the cronjob, determine the target date for the metrics snapshot generation
    'days_ago' => 1,

    // logging
    'log_channel' => 'stack',

    // skip built-in migrations
    'skip_package_migrations' => true,
];
