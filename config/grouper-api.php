<?php

return [
    // OPTIONS

    'use_singleton' => true,

    'notification_mode' => [], // flash, log, or empty array for none

    'debug_mode' => false, // true - extra logging in Barryvdh Debugbar (required)

    // URLs and CREDENTIALS

    'host'     => env('GROUPER_API_HOST'),
    'username' => env('GROUPER_API_USERNAME'),
    'password' => env('GROUPER_API_PASSWORD'),

    // API SETTINGS

    'results_per_page' => env('GROUPER_API_RESULTS_PER_PAGE', 50),

    'subject_attribute_names' => explode(',', env('GROUPER_API_SUBJECT_ATTRIBUTE_NAMES', 'description,name')),

    'person_identifiers' => explode(',', env('GROUPER_API_PERSON_IDENTIFIERS', 'person')),
    'group_identifiers'  => explode(',', env('GROUPER_API_GROUP_IDENTIFIERS', 'group')),


    // CACHE

    'cache_active'  => env('GROUPER_API_CACHING', 'on') == 'on', // set to 'on' or 'off' in .env file
    'cache_minutes' => env('GROUPER_API_CACHE_MINUTES', 10),
    'cache_methods' => [
        // enter method names from the Uncgits\Grouper\GrouperApi object that can be cached, for instance 'getMembershipsLite'
    ],

    // PROXY

    'http_proxy' => [
        'enabled' => env('GROUPER_API_USE_PROXY', false) == 'true',
        'host'    => env('GROUPER_API_PROXY_HOST', null),
        'port'    => env('GROUPER_API_PROXY_PORT', null),
    ],
];
