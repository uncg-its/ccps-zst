<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Log Channel
    |--------------------------------------------------------------------------
    |
    | This option defines the default log channel that gets used when writing
    | messages to the logs. The name specified in this option should match
    | one of the channels defined in the "channels" configuration array.
    |
    */

    'default' => env('LOG_CHANNEL', 'stack'),

    /*
    |--------------------------------------------------------------------------
    | Log Channels
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log channels for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Drivers: "single", "daily", "slack", "syslog",
    |                    "errorlog", "custom", "stack"
    |
    */
    'channels' => [
        'stack' => [
            'driver'   => 'stack',
            'channels' => ['default'],
        ],

        'default' => [
            'driver' => 'daily',
            'path'   => storage_path('logs/laravel.log'),
            'level'  => 'debug',
            'days'   => env('APP_LOG_MAX_FILES', 7)
        ],

        /* Examples

        'daily' => [
            'driver' => 'daily',
            'path' => storage_path('logs/laravel.log'),
            'level' => 'debug',
            'days' => 7,
        ],

        'slack' => [
            'driver' => 'slack',
            'url' => env('LOG_SLACK_WEBHOOK_URL'),
            'username' => 'Laravel Log',
            'emoji' => ':boom:',
            'level' => 'critical',
        ],

        'syslog' => [
            'driver' => 'syslog',
            'level' => 'debug',
        ],

        'errorlog' => [
            'driver' => 'errorlog',
            'level' => 'debug',
        ],
        */


        /*
         * Default CCPS Core channels
         */

        'general'        => [
            'driver'   => 'stack',
            'channels' => ['general-local', 'general-splunk']
        ],
        'general-local'  => [
            'driver' => 'daily',
            'path'   => storage_path('logs/general.log'),
            'level'  => 'debug',
            'days'   => env('APP_LOG_MAX_FILES', 7)
        ],
        'general-splunk' => [
            'driver'    => 'daily',
            'path'      => storage_path('logs/splunk/general.log'),
            'level'     => 'info',
            'days'      => env('APP_LOG_MAX_FILES_SPLUNK', 3),
            'formatter' => \Uncgits\Ccps\Components\Log\Formatter\SplunkJsonFormatter::class,
        ],

        'queue'        => [
            'driver'   => 'stack',
            'channels' => ['queue-local', 'queue-splunk']
        ],
        'queue-local'  => [
            'driver' => 'daily',
            'path'   => storage_path('logs/queue.log'),
            'level'  => 'debug',
            'days'   => env('APP_LOG_MAX_FILES', 7)
        ],
        'queue-splunk' => [
            'driver'    => 'daily',
            'path'      => storage_path('logs/splunk/queue.log'),
            'level'     => 'info',
            'days'      => env('APP_LOG_MAX_FILES_SPLUNK', 3),
            'formatter' => \Uncgits\Ccps\Components\Log\Formatter\SplunkJsonFormatter::class,
        ],

        'cron'        => [
            'driver'   => 'stack',
            'channels' => ['cron-local', 'cron-splunk']
        ],
        'cron-local'  => [
            'driver' => 'daily',
            'path'   => storage_path('logs/cron.log'),
            'level'  => 'debug',
            'days'   => env('APP_LOG_MAX_FILES', 7)
        ],
        'cron-splunk' => [
            'driver'    => 'daily',
            'path'      => storage_path('logs/splunk/cron.log'),
            'level'     => 'info',
            'days'      => env('APP_LOG_MAX_FILES_SPLUNK', 3),
            'formatter' => \Uncgits\Ccps\Components\Log\Formatter\SplunkJsonFormatter::class,
        ],

        'access'        => [
            'driver'   => 'stack',
            'channels' => ['access-local', 'access-splunk']
        ],
        'access-local'  => [
            'driver' => 'daily',
            'path'   => storage_path('logs/access.log'),
            'level'  => 'debug',
            'days'   => env('APP_LOG_MAX_FILES', 7)
        ],
        'access-splunk' => [
            'driver'    => 'daily',
            'path'      => storage_path('logs/splunk/access.log'),
            'level'     => 'info',
            'days'      => env('APP_LOG_MAX_FILES_SPLUNK', 3),
            'formatter' => \Uncgits\Ccps\Components\Log\Formatter\SplunkJsonFormatter::class,
        ],

        'database'        => [
            'driver'   => 'stack',
            'channels' => ['database-local', 'database-splunk']
        ],
        'database-local'  => [
            'driver' => 'daily',
            'path'   => storage_path('logs/database.log'),
            'level'  => 'debug',
            'days'   => env('APP_LOG_MAX_FILES', 7)
        ],
        'database-splunk' => [
            'driver'    => 'daily',
            'path'      => storage_path('logs/splunk/database.log'),
            'level'     => 'info',
            'days'      => env('APP_LOG_MAX_FILES_SPLUNK', 3),
            'formatter' => \Uncgits\Ccps\Components\Log\Formatter\SplunkJsonFormatter::class,
        ],

        'acl'        => [
            'driver'   => 'stack',
            'channels' => ['acl-local', 'acl-splunk']
        ],
        'acl-local'  => [
            'driver' => 'daily',
            'path'   => storage_path('logs/acl.log'),
            'level'  => 'debug',
            'days'   => env('APP_LOG_MAX_FILES', 7)
        ],
        'acl-splunk' => [
            'driver'    => 'daily',
            'path'      => storage_path('logs/splunk/acl.log'),
            'level'     => 'info',
            'days'      => env('APP_LOG_MAX_FILES_SPLUNK', 3),
            'formatter' => \Uncgits\Ccps\Components\Log\Formatter\SplunkJsonFormatter::class,
        ],

        'backup'        => [
            'driver'   => 'stack',
            'channels' => ['backup-local', 'backup-splunk']
        ],
        'backup-local'  => [
            'driver' => 'daily',
            'path'   => storage_path('logs/backup.log'),
            'level'  => 'debug',
            'days'   => env('APP_LOG_MAX_FILES', 7)
        ],
        'backup-splunk' => [
            'driver'    => 'daily',
            'path'      => storage_path('logs/splunk/backup.log'),
            'level'     => 'info',
            'days'      => env('APP_LOG_MAX_FILES_SPLUNK', 3),
            'formatter' => \Uncgits\Ccps\Components\Log\Formatter\SplunkJsonFormatter::class,
        ],

        'notifications'        => [
            'driver'   => 'stack',
            'channels' => ['notifications-local', 'notifications-splunk']
        ],
        'notifications-local'  => [
            'driver' => 'daily',
            'path'   => storage_path('logs/notifications.log'),
            'level'  => 'debug',
            'days'   => env('APP_LOG_MAX_FILES', 7)
        ],
        'notifications-splunk' => [
            'driver'    => 'daily',
            'path'      => storage_path('logs/splunk/notifications.log'),
            'level'     => 'info',
            'days'      => env('APP_LOG_MAX_FILES_SPLUNK', 3),
            'formatter' => \Uncgits\Ccps\Components\Log\Formatter\SplunkJsonFormatter::class,
        ],

        'metrics'        => [
            'driver'   => 'stack',
            'channels' => ['metrics-local', 'metrics-splunk']
        ],
        'metrics-local'  => [
            'driver' => 'daily',
            'path'   => storage_path('logs/metrics.log'),
            'level'  => 'debug',
            'days'   => env('APP_LOG_MAX_FILES', 7)
        ],
        'metrics-splunk' => [
            'driver'    => 'daily',
            'path'      => storage_path('logs/splunk/metrics.log'),
            'level'     => 'info',
            'days'      => env('APP_LOG_MAX_FILES_SPLUNK', 3),
            'formatter' => \Uncgits\Ccps\Components\Log\Formatter\SplunkJsonFormatter::class,
        ],

        'application-snapshots'        => [
            'driver'   => 'stack',
            'channels' => ['application-snapshots-local', 'application-snapshots-splunk']
        ],
        'application-snapshots-local'  => [
            'driver' => 'daily',
            'path'   => storage_path('logs/application-snapshots.log'),
            'level'  => 'debug',
            'days'   => env('APP_LOG_MAX_FILES', 7)
        ],
        'application-snapshots-splunk' => [
            'driver'    => 'daily',
            'path'      => storage_path('logs/splunk/application-snapshots.log'),
            'level'     => 'info',
            'days'      => env('APP_LOG_MAX_FILES_SPLUNK', 3),
            'formatter' => \Uncgits\Ccps\Components\Log\Formatter\SplunkJsonFormatter::class,
        ],

        'exceptions'        => [
            'driver'   => 'stack',
            'channels' => ['exceptions-local', 'exceptions-splunk']
        ],
        'exceptions-local'  => [
            'driver' => 'daily',
            'path'   => storage_path('logs/exceptions.log'),
            'level'  => 'debug',
            'days'   => env('APP_LOG_MAX_FILES', 7)
        ],
        'exceptions-splunk' => [
            'driver'    => 'daily',
            'path'      => storage_path('logs/splunk/exceptions.log'),
            'level'     => 'info',
            'days'      => env('APP_LOG_MAX_FILES_SPLUNK', 3),
            'formatter' => \Uncgits\Ccps\Components\Log\Formatter\SplunkJsonFormatter::class,
        ],
    ],
];
