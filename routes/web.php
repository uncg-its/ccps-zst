<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'CcpsCore\HomeController@index')->name('home');
Route::get('home', 'CcpsCore\HomeController@index');

// CCPS ZST
Route::prefix('metrics')->name('metrics.')->group(function () {
    Route::view('/', 'metrics.index')->name('index');
    // metrics snapshots
    Route::prefix('snapshots')->name('snapshots.')->group(function () {
        Route::get('/', 'MetricsSnapshotController@index')->name('index');
        Route::get('create', 'MetricsSnapshotController@create')->name('create');
        Route::post('/', 'MetricsSnapshotController@store')->name('store');
        Route::get('{metrics_snapshot}', 'MetricsSnapshotController@show')->name('show');
        Route::delete('{metrics_snapshot}', 'MetricsSnapshotController@destroy')->name('destroy');
        Route::post('{metrics_snapshot}/restore', 'MetricsSnapshotController@restore')->name('restore');
        Route::post('{metrics_snapshot}/push', 'MetricsSnapshotController@push')->name('push');
    });

    Route::prefix('backfills')->name('backfills.')->group(function () {
        Route::get('/', 'MetricsBackfillController@index')->name('index');
        Route::get('/create', 'MetricsBackfillController@create')->name('create');
        Route::post('/create', 'MetricsBackfillController@store')->name('store');
    });
});

// API
Route::prefix('live')->name('live.')->group(function () {
    Route::view('/', 'live.index')->name('index');
    Route::prefix('scheduled-meetings')->name('scheduled-meetings.')->group(function () {
        Route::get('/', 'ScheduledMeetingController@index')->name('index');
        Route::get('{id}', 'ScheduledMeetingController@show')->name('show');
    });
    Route::prefix('scheduled-webinars')->name('scheduled-webinars.')->group(function () {
        Route::get('/', 'ScheduledWebinarController@index')->name('index');
        Route::get('{id}', 'ScheduledWebinarController@show')->name('show');
    });

    Route::prefix('users')->name('users.')->group(function () {
        Route::get('/', 'UserController@index')->name('index');
    });
    Route::prefix('meetings')->name('meetings.')->group(function () {
        Route::get('/', 'MeetingController@index')->name('index');
    });
});

// CCPS CORE

Route::group(['prefix' => 'account'], function () {
    Route::get('/', 'CcpsCore\AccountController@index')->name('account');
    Route::get('/settings', 'CcpsCore\AccountController@settings')->name('account.settings');
    Route::post('/settings', 'CcpsCore\AccountController@updateSettings')->name('account.settings.update');
    Route::get('/profile', 'CcpsCore\AccountController@profile')->name('profile.show');
    Route::get('/profile/edit', 'CcpsCore\AccountController@editProfile')->name('profile.edit');
    Route::patch('/profile/', 'CcpsCore\AccountController@updateProfile')->name('profile.update');

    Route::prefix('tokens')->name('account.tokens.')->group(function () {
        Route::get('/', 'CcpsCore\AccountController@tokens')->name('index');
        Route::get('create', 'CcpsCore\AccountController@createToken')->name('create');
        Route::post('/', 'CcpsCore\AccountController@storeToken')->name('store');
        Route::get('{token}', 'CcpsCore\AccountController@editToken')->name('edit');
        Route::patch('{token}', 'CcpsCore\AccountController@updateToken')->name('update');
        Route::delete('{token}', 'CcpsCore\AccountController@revokeToken')->name('revoke');
    });

    Route::group(['prefix' => 'notifications'], function () {
        Route::get('/', 'CcpsCore\AccountController@notifications')->name('account.notifications.index');
        Route::get('/channels/create', 'CcpsCore\AccountController@createChannel')->name('account.notifications.channels.create');
        Route::post('/channels/create', 'CcpsCore\AccountController@storeChannel')->name('account.notifications.channels.store');
        Route::delete('/channels/{channel}', 'CcpsCore\AccountController@destroyChannel')->name('account.notifications.channels.destroy');
        Route::get(
            '/channels/{channel}/resend-verification',
            'CcpsCore\ChannelVerificationController@resend'
        )->name('account.notifications.channels.resend-verification');
        Route::get(
            '/channels/{channel}/verify',
            'CcpsCore\ChannelVerificationController@verify'
        )->name('account.notifications.channels.verify');
        Route::get(
            '/channels/{channel}/test',
            'CcpsCore\ChannelVerificationController@test'
        )->name('account.notifications.channels.test');

        Route::get('/configure', 'CcpsCore\AccountController@configure')->name('account.notifications.configure');
        Route::post('/configure', 'CcpsCore\AccountController@configureSave')->name('account.notifications.configure.save');
    });
});

// CCPS Core routes
Uncgits\Ccps\Support\Facades\CcpsCore::routes();

// For local auth
Auth::routes();


// Socialite - User Account OAuth Routes
Route::get('auth/{provider}', 'CcpsCore\AuthController@redirectToProvider')->name('oauth');
Route::get('auth/{provider}/callback', 'CcpsCore\AuthController@handleProviderCallback')->name('oauth.callback');

// Horizon override
Route::group([
    'prefix'     => 'horizon',
    'namespace'  => 'Laravel\Horizon\Http\Controllers',
    'middleware' => config('horizon.middleware', 'web'),
], function () {
    // Catch-all Route...
    Route::get('/{view?}', '\App\Http\Controllers\CcpsCore\HomeController@horizon')->where(
        'view',
        '(.*)'
    )->name('horizon.index');
});
