# CCPS Zoom Support Tool (ZST)

This application provides administrative support tools and metrics generation for the Zoom service at UNCG

# Version History

## 1.6.1

- migration to add `customer_key` field to archived participants table

## 1.6.0

- CCPS Core 2.3

## 1.5.2

- fix for bs4 forms `fill()` method

## 1.5.1

- Patch to fix some errors in passing data to bootstrap forms package

## 1.5.0

- CCPS Core 2.2.*
- Laravel 8.0+
- PHP 8 readiness

## 1.4.6

- adds new column `status` to `archived_participants` table per API changelog documentation

## 1.4.5

- adds new columns to `archived_users` and `archived_recordings` tables per API changelog documentation

## 1.4.4

- give default value to `in_room_participants` column

## 1.4.3

- adds the `in_room_participants` column to `archived_participants` table, so that the response from the ArchiveParticipants job does not fail to save to the database.

## 1.4.2

- Change from throwing exceptions on 404 error when archiving participants to an acceptable result (log in DB, exit with success code)

## 1.4.1

- Meeting lookup fix for missing array indexes

## 1.4

- Upcoming meeting lookup by ID

## 1.3.3

- Fix a mixup in metric dimension and metric name
- Fix the non-generation of session-length metrics

## 1.3.2

- Introduce float casting in metrics calculation, so that we don't end up with a string.

## 1.3.1

- Update to Laravel Framework ^7.22 (security patch)

## 1.3

- CCPS Bot endpoints

## 1.2

- Metrics Snapshots redone, and Metrics Clearing House Pusher

## 1.1.2

- Fix for error that shows up if avatar url is missing for a user

## 1.1.1

- User Lookup tool transitioned to Livewire

## 1.1

- User Lookup tool

## 1.0

- Official versioning scheme using semver
- CCPS Core 2.0
