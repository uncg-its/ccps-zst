<?php

namespace App\Repositories;

use App\MetricsSnapshot;
use App\Traits\ParsesIntoCarbonFormat;
use Uncgits\Ccps\Repositories\Traits\EloquentCrud;

class MetricsSnapshotRepository
{
    use EloquentCrud, ParsesIntoCarbonFormat;

    public $modelName = MetricsSnapshot::class;

    public $searchable = [
        'date'       => [
            'operand' => '=',
        ],
        'target_date_start' => [
            'operand' => '>',
            'column'  => 'date'
        ],
        'target_date_end' => [
            'operand' => '<=',
            'column'  => 'date'
        ]
    ];

    public function forDay($day)
    {
        $day = $this->parseIntoCarbon($day);

        return $this->search(['date' => $day->format('Y-m-d')])
            ->first();
    }

    public function existsForDay($day)
    {
        $day = $this->parseIntoCarbon($day);

        return $this->search(['date' => $day->format('Y-m-d')])
            ->count() > 0;
    }

    public function unpushed()
    {
        return MetricsSnapshot::whereNull('pushed_at')->get();
    }
}
