<?php

namespace App\Notifications;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Uncgits\Ccps\Models\Cronjob;
use Illuminate\Notifications\Notification;
use Uncgits\Ccps\Messages\GoogleChatMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\SlackMessage;

class CronjobFailed extends Notification
{
    use Queueable;

    protected $cronjob;
    protected $exception;
    protected $message;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Cronjob $cronjob, \Exception $exception)
    {
        $this->cronjob = $cronjob;
        $this->exception = $exception;

        $this->message = 'The Cron Job with name ' . $this->cronjob->getDisplayName() . ' has FAILED at ' . Carbon::now()->toDateTimeString() . '. Error message: ' . $this->exception->getMessage();
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return $notifiable->channel;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if ($notifiable->type == 'sms') {
            return $this->toSms($notifiable);
        }

        return (new MailMessage)
            ->markdown('emails.cronjob-failed', ['message' => $this->message]);
    }

    public function toGoogleChat($notifiable)
    {
        return (new GoogleChatMessage)
            ->content($this->message)
            ->to($notifiable->key);
    }

    public function toSms($notifiable)
    {
        return (new MailMessage)
            ->markdown('emails.plaintext.cronjob-failed', ['message' => $this->message]);
    }

    public function toSlack($notifiable)
    {
        // is the proxy needed?
        $proxyValue = config('ccps.http_proxy.enabled')
            ? config('ccps.http_proxy.host') . ':' . config('ccps.http_proxy.port')
            : '';

        return (new SlackMessage)
            ->http(['proxy' => $proxyValue])
            ->error()
            ->content(':boom: Cron Job FAILED!')
            ->attachment(function ($attachment) {
                $attachment->title('Cron Job Details')
                ->fields([
                    'Name'      => $this->cronjob->getDisplayName(),
                    'Failed at' => Carbon::now()->toDateTimeString(),
                    'Message'   => $this->exception->getMessage(),
                ]);
            });
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
