<?php

namespace App\Exceptions;

use Throwable;
use Illuminate\Database\QueryException;
use App\Events\CcpsCore\UncaughtQueryException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Throwable  $throwable
     * @return void
     */
    public function report(Throwable $throwable)
    {
        // log uncaught query exceptions but continue processing.
        if ($throwable instanceof QueryException) {
            event(new UncaughtQueryException($throwable));
        }

        // log to exception reporting channel(s)
        if (config('ccps.exceptions.should_log')) {
            \Log::channel(config('ccps.exceptions.log_channel'))->error($throwable->getMessage(), [
                'category'  => 'exception',
                'operation' => 'exception',
                'result'    => 'exception',
                'data'      => [
                    'exception' => $throwable
                ]
            ]);
        }

        parent::report($throwable);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $throwable
     * @return \Illuminate\Http\Response
     */
    public function render($request, Throwable $throwable)
    {
        // defer to default for JSON responses
        if ($request->expectsJson()) {
            return parent::render($request, $throwable);
        }

        // redirect to login if 403
        if (config('ccps.redirect_to_login') && $throwable instanceof HttpException) {
            if ($throwable->getStatusCode() === 403) {
                if (auth()->user()) {
                    return response()->view('errors.403', ['exception' => $throwable], 403);
                }
                flash('You must be logged in to access this page.')->warning();
                return redirect()->route('login');
            }
        }

        // fallback to catch any edge cases we haven't accounted for.
        return parent::render($request, $throwable);
    }
}
