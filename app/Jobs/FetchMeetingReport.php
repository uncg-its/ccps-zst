<?php

namespace App\Jobs;

use App\ArchivedMeeting;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class FetchMeetingReport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $meeting;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($meeting)
    {
        if (is_a($meeting, ArchivedMeeting::class)) {
            $this->meeting = $meeting;
        } else {
            $this->meeting = ArchivedMeeting::findOrFail($meeting); //uuid
        }

        $this->onQueue('api');

        $this->tries = 25;
        $this->maxExceptions = 1;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Redis::throttle('api.heavy')
            ->allow(7)
            ->every(1)
            ->then(function () {
                $report = \ZoomApi::using('reports')
                    ->getMeetingDetailReports($this->meeting->encoded_uuid);

                if ($report->status() !== 'success') {
                    if ($report->lastCode() === 429) {
                        // rate limiting that we shouldn't hit, but Zoom's docs are very murky right now
                        $this->release(60);
                        return;
                    }

                    $message = 'API Error: ' . $report->lastCode() . ' - ' . $report->lastReason();
                    $this->meeting->report_fetched_message = $message;
                    $this->meeting->save();

                    throw new \Exception($message);
                }

                $this->meeting->total_minutes = $report->content()['total_minutes'];
                $this->meeting->report_fetched_at = now();
                $this->meeting->report_fetched_message = null;
                $this->meeting->save();
            }, function () {
                $this->release(20);
            });
    }
}
