<?php

namespace App\Jobs;

use App\ArchivedMeeting;
use App\ArchivedRecording;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class FetchMeetingRecordings implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $meeting;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(ArchivedMeeting $meeting)
    {
        $this->onQueue('api');
        $this->meeting = $meeting;

        $this->tries = 25;
        $this->maxExceptions = 1;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // NOTE - don't need this anymore, unless we go back to the per-meeting method

        \Redis::throttle('api.medium')
            ->allow(40)
            ->every(1)
            ->then(function () {
                $recordings = \ZoomApi::using('CloudRecording')
                    ->getMeetingRecordings($this->meeting->encoded_uuid);

                if ($recordings->status() !== 'success') {
                    $message = 'API Error: ' . $recordings->lastCode() . ' - ' . $recordings->lastReason();
                    $this->meeting->recordings_fetched_message = $message;
                    $this->meeting->save();

                    throw new \Exception($message);
                }

                \DB::transaction(function () use ($recordings) {
                    $files = collect($recordings->content()['recording_files']);

                    $files->each(function ($recording) {
                        ArchivedRecording::updateOrCreate(['id' => $recording->id], (array) $recording);
                    });

                    $this->meeting->recordings_fetched_message = null;
                    $this->meeting->recordings_fetched_at = now();
                    $this->meeting->save();
                });
            }, function () {
                // Could not obtain lock; job will be re-queued
                return $this->release(10);
            });
    }
}
