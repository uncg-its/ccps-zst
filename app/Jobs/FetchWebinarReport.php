<?php

namespace App\Jobs;

use App\ArchivedWebinar;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class FetchWebinarReport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $webinar;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($webinar)
    {
        if (is_a($webinar, ArchivedWebinar::class)) {
            $this->webinar = $webinar;
        } else {
            $this->webinar = ArchivedWebinar::findOrFail($webinar); //uuid
        }

        $this->onQueue('api');

        $this->tries = 25;
        $this->maxExceptions = 1;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Redis::throttle('api.heavy')
            ->allow(7)
            ->every(1)
            ->then(function () {
                $report = \ZoomApi::using('reports')
                    ->getWebinarDetailReports($this->webinar->encoded_uuid);

                if ($report->status() !== 'success') {
                    if ($report->lastCode() === 429) {
                        // rate limiting that we shouldn't hit, but Zoom's docs are very murky right now
                        $this->release(60);
                        return;
                    }

                    $message = 'API Error: ' . $report->lastCode() . ' - ' . $report->lastReason();
                    $this->webinar->report_fetched_message = $message;
                    $this->webinar->save();

                    throw new \Exception($message);
                }

                $this->webinar->total_minutes = $report->content()['total_minutes'];
                $this->webinar->report_fetched_at = now();
                $this->webinar->report_fetched_message = null;
                $this->webinar->save();
            }, function () {
                $this->release(20);
            });
    }
}
