<?php

namespace App\Jobs;

use App\ArchivedMeeting;
use App\ArchivedWebinar;
use App\Contracts\ArchivedSession;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;

class FetchParticipantReport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $session;
    protected $participants;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(ArchivedSession $session, Collection $participants)
    {
        $this->session = $session;
        $this->participants = $participants;
        $this->onQueue('api');

        $this->tries = 25;
        $this->maxExceptions = 1;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Redis::throttle('api.heavy')
            ->allow(7)
            ->every(1)
            ->then(function () {
                if (is_a($this->session, ArchivedMeeting::class)) {
                    $report = \ZoomApi::using('reports')
                        ->getMeetingParticipantReports($this->session->encoded_uuid);
                } elseif (is_a($this->session, ArchivedWebinar::class)) {
                    $report = \ZoomApi::using('reports')
                        ->getWebinarParticipantReports($this->session->encoded_uuid);
                } else {
                    throw new \Exception(get_class($this->session) . ' is not a valid ArchivedMeeting or ArchivedWebinar model');
                }

                if ($report->status() !== 'success') {
                    if ($report->lastCode() === 429) {
                        // rate limiting that we shouldn't hit, but Zoom's docs are very murky right now
                        $this->release(60);
                        return;
                    }

                    $message = 'API Error: ' . $report->lastCode() . ' - ' . $report->lastReason();
                    $this->participants->each(function ($participant) use ($message) {
                        $participant->report_fetched_message = $message;
                        $participant->save();
                    });

                    throw new \Exception($message);
                }

                $report->content()->each(function ($item) {
                    $recordToUpdate = $this->participants->where('user_id', $item->user_id)
                        ->where('attendable_id', $this->session->uuid)
                        ->first();

                    if (is_null($recordToUpdate)) {
                        // it was a call-in record for another user already in the session info. we can disregard.
                        return;
                    }

                    $recordToUpdate->duration = $item->duration;
                    $recordToUpdate->report_fetched_at = now();
                    $recordToUpdate->save();
                });
            }, function () {
                $this->release(20);
            });
    }
}
