<?php

namespace App\Jobs;

use App\ArchivedMeeting;
use App\ArchivedParticipant;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class FetchMeetingParticipants implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $meeting;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(ArchivedMeeting $meeting)
    {
        $this->onQueue('api');
        $this->meeting = $meeting;

        $this->tries = 25;
        $this->maxExceptions = 1;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Redis::throttle('api.resource-intensive')
            ->allow(10)
            ->every(60)
            ->then(function () {
                $participantsResult = \ZoomApi::using('dashboards')
                    ->addParameters(['type' => 'past'])
                    ->listMeetingParticipants($this->meeting->encoded_uuid);

                if ($participantsResult->status() !== 'success') {
                    if ($participantsResult->lastCode() === 429) {
                        // api rate limit, release and retry later
                        $this->release(60);
                    }

                    if ($participantsResult->lastCode() === 404) {
                        // this is an acceptable failure
                        $this->meeting->participants_fetched_message = 'Meeting not found; skipped';
                        $this->meeting->participants_fetched_at = now();
                        $this->meeting->save();
                        return;
                    }

                    $message = 'API Error: ' . $participantsResult->lastCode() . ' - ' . $participantsResult->lastReason();
                    $this->meeting->participants_fetched_message = $message;
                    $this->meeting->save();

                    throw new \Exception($message);
                }

                try {
                    \DB::beginTransaction();

                    $participants = $participantsResult->content();

                    $archivedParticipants = $participants->map(function ($participant) {
                        $participantData = (array) $participant;
                        $participantData['attendable_id'] = $this->meeting->uuid;
                        if (isset($participantData['id'])) {
                            $participantData['zoom_id'] = $participantData['id'];
                            unset($participantData['id']);
                        }
                        $participantData['attendable_type'] = ArchivedMeeting::class;

                        return ArchivedParticipant::updateOrCreate(
                            ['user_id' => $participantData['user_id'], 'attendable_id' => $participantData['attendable_id']],
                            $participantData
                        );
                    });

                    $this->meeting->participants_fetched_message = null;
                    $this->meeting->participants_fetched_at = now();
                    $this->meeting->save();

                    \DB::commit();

                    dispatch(new FetchParticipantReport($this->meeting, $archivedParticipants))->delay(now()->addSeconds(15));
                } catch (\Throwable $th) {
                    \DB::rollBack();
                    throw $th;
                }
            }, function () {
                // Could not obtain lock; job will be re-queued
                return $this->release(60);
            });
    }
}
