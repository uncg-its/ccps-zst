<?php

namespace App\Services;

use Carbon\Carbon;
use App\ArchivedUser;
use App\ArchivedReport;
use App\ArchivedMeeting;
use App\MetricsSnapshot;
use App\ArchivedRecording;
use App\ArchivedParticipant;
use App\Exceptions\MetricsSnapshotException;
use App\Traits\ParsesIntoCarbonFormat;
use App\Repositories\MetricsSnapshotRepository;
use Illuminate\Support\Facades\Http;

class MetricsSnapshotServiceOld
{
    use ParsesIntoCarbonFormat;

    public function generateForDay($day)
    {
        if (!is_a($day, Carbon::class)) {
            $day = Carbon::parse($day);
        }

        // REPORTS for day
        $reportsForDay = ArchivedReport::where('date', $day->format('Y-m-d'))->get();
        $cloudRecordingStorageReport = $reportsForDay->where('type', 'cloud_recording_storage')->first();
        $dailyUsageReport = $reportsForDay->where('type', 'daily_usage')->first();

        // USERS
        $activeUserCount = ArchivedUser::count();
        $newUsers = $dailyUsageReport->data['new_users'];


        // RECORDINGS
        $recordingsOnDay = ArchivedRecording::createdOnDay($day)
            ->select('id', 'zoom_id', 'file_type')
            ->where('file_type', 'MP4')
            ->count();

        $recordingStorageUsed = $cloudRecordingStorageReport->data['usage'];
        // parse into GB
        $recordingStorageParsed = explode(' ', $recordingStorageUsed);
        $recordingGbUsed = $recordingStorageParsed[0]; // assume GB since that's the unit we want

        if ($recordingStorageParsed[1] === 'MB') {
            $recordingGbUsed = 1 / $recordingStorageParsed[0];
        } elseif ($recordingStorageParsed[1] === 'TB') {
            $recordingGbUsed = $recordingStorageParsed[0] * 1000; // or is it 1024?
        }

        // MEETINGS
        $meetingsOnDay = ArchivedMeeting::startedOnDay($day)
            ->select('uuid', 'start_time', 'end_time')
            ->get();

        $numberOfMeetingsForDay = $meetingsOnDay->count();

        $numberOfMeetingsWithRecording = ArchivedMeeting::startedOnDay($day)
            ->has('archived_recordings')
            ->count();

        // $meetingMinutes = $dailyUsageReport->data->meeting_minutes; // nope.
        $meetingMinutes = $meetingsOnDay->sum(function ($meeting) {
            return $meeting->start_time->diffInSeconds($meeting->end_time);
        }) / 60;

        // PARTICIPANTS
        $participantsOnDay = ArchivedParticipant::joinedOnDay($day)
            ->select('id', 'zoom_id', 'duration', 'user_name')
            ->get();

        $peopleMinutes = $participantsOnDay->sum('duration') / 60;

        // MEETING LENGTH CRUNCHING
        $meetingLengthThresholds = config('zst.metrics.meeting_length_thresholds');

        // init array
        $meetingLengthCount = [];

        foreach ($meetingLengthThresholds as $index => $limit) {
            $upperBoundIndex = $index + 1;

            if (!isset($meetingLengthThresholds[$upperBoundIndex])) {
                $upperBound = 99999;
                $upperBoundText = "OrHigher";
            } else {
                $upperBound = $meetingLengthThresholds[$upperBoundIndex];
                $upperBoundText = "To" . ($upperBound - 1);
            }

            $lengthKey = "numberOfMeetingsWithLength" . $limit . $upperBoundText;

            if (!isset($meetingLengthCount[$lengthKey])) {
                $meetingLengthCount[$lengthKey] = 0;
            }

            // grab the count
            $meetingLengthCount[$lengthKey] = $meetingsOnDay->filter(function ($meeting) use ($limit, $upperBound) {
                $duration = $meeting->start_time->diffInMinutes($meeting->end_time);
                return $duration >= $limit && $duration < $upperBound;
            })->count();
        }


        $rawData = [
            'numberOfUsers'                 => $activeUserCount,
            'numberOfNewUsers'              => $newUsers,
            'numberOfMeetings'              => $numberOfMeetingsForDay,
            'numberOfMeetingsWithRecording' => $numberOfMeetingsWithRecording,
            'numberOfParticipants'          => $participantsOnDay->count(),
            'numberOfParticipantsUnique'    => $participantsOnDay->unique('user_name')->count(),
            'numberOfMobileAppUsers'        => ArchivedParticipant::joinedOnDay($day)->joinedFromMobile()->count(),
            'numberOfChromeOsUsers'         => ArchivedParticipant::joinedOnDay($day)->joinedFromChromeOs()->count(),
            'numberOfWebAppUsers'           => ArchivedParticipant::joinedOnDay($day)->joinedFromWeb()->count(),
            'numberOfCallInUsers'           => ArchivedParticipant::joinedOnDay($day)->calledIn()->count(),
            'numberOfWifiUsers'             => ArchivedParticipant::joinedOnDay($day)->onWifi()->count(),
            'numberOfWiredUsers'            => ArchivedParticipant::joinedOnDay($day)->onWired()->count(),
            'numberOfCellularDataUsers'     => ArchivedParticipant::joinedOnDay($day)->onCellularData()->count(),
            'meetingMinutes'                => round($meetingMinutes, 2),
            'peopleMinutes'                 => round($peopleMinutes, 2),
            'numberOfRecordingsMade'        => $recordingsOnDay,
            'recordingGbUsed'               => $recordingGbUsed,
        ];

        $rawData = array_merge($rawData, $meetingLengthCount);

        // create model
        return (new MetricsSnapshotRepository)->create([
            'date'             => $day->format('Y-m-d'),
            'raw_data'         => $rawData,
            'created_at'       => now(),
            'push_destination' => config('zst.metrics.push_destinations')
        ]);
    }

    public function pushToMetricsClearingHouse(MetricsSnapshot $snapshot)
    {
        $apiSuccess = true;

        foreach ($snapshot->raw_data as $key => $value) {
            $dataToSend = [
                'service_name'     => config('zst.metrics.service_name'),
                'metric_name'      => $key,
                'metric_value'     => $value,
                'metric_timestamp' => Carbon::parse($snapshot->date)->format('Y-m-d\TH:i:sO')
            ];

            $metricsApiCall = Http::withHeaders([
                'Content-Type' => 'application/json',
                'Accept'       => 'application/json',
            ])->withToken(config('zst.metrics.mch.api_key'))
                ->post(config('zst.metrics.mch.endpoint'), $dataToSend);

            if (! $metricsApiCall->successful()) {
                $apiSuccess = false;

                \Log::channel('metrics')->warning('API failure while pushing snapshot to Metrics Clearinghouse', [
                    'category'  => 'metrics',
                    'operation' => 'push',
                    'result'    => 'failure',
                    'data'      => [
                        'snapshot_id' => $snapshot->id,
                        'sent_data'   => $dataToSend,
                        'response'    => $metricsApiCall
                    ]
                ]);
            }
        }

        if ($apiSuccess) {
            $snapshot->pushed_at = Carbon::now()->toDateTimeString();
            $snapshot->error_data = null;
            $snapshot->save();

            \Log::channel('metrics')->info('Snapshot pushed to Metrics Clearinghouse successfully', [
                'category'  => 'metrics',
                'operation' => 'push',
                'result'    => 'success',
                'data'      => [
                    'snapshot_id' => $snapshot->id,
                    'destination' => 'metrics-clearing-house'
                ]
            ]);

            return true;
        }

        $snapshot->pushed_at = Carbon::now()->toDateTimeString();
        $snapshot->error_data = 'metrics-clearing-house: One or more metric pushes encountered an error.';
        $snapshot->save();

        throw new MetricsSnapshotException('One or more metric pushes encountered an error. Please check app logs.');
    }

    // public function pushToSplunk(MetricsSnapshot $snapshot)
    // {
    //     $apiSuccess = true;

    //     foreach ($snapshot->raw_data as $key => $value) {
    //         $dataToSend = [];

    //         $fieldsArray = [
    //             'metric_name'   => config('canvas-feed.metrics.prefix') . '.' . $key,
    //             '_value'        => $value,
    //         ];

    //         $thisArray = [
    //             'time'          => strtotime($snapshot->date),
    //             'index'         => config('canvas-feed.metrics.splunk.index'),
    //             'source'        => config('canvas-feed.metrics.splunk.source'),
    //             'host'          => config('canvas-feed.metrics.host'),
    //             'event'         => 'metric',
    //             'fields'        => $fieldsArray,
    //         ];

    //         $dataToSend[] = $thisArray;

    //         // dump($dataToSend);

    //         $metricsApiCall = \SplunkApi::sendData($dataToSend);

    //         // dump($metricsApiCall);

    //         if ($metricsApiCall['response']['httpCode'] != '200') {
    //             $apiSuccess = false;

    //             \Log::channel('metrics')->warning('API failure while pushing snapshot to Splunk', [
    //                 'category'  => 'metrics',
    //                 'operation' => 'push',
    //                 'result'    => 'failure',
    //                 'data'      => [
    //                     'snapshot_id' => $snapshot->id,
    //                     'sent_data'   => $dataToSend,
    //                     'response'    => $metricsApiCall
    //                 ]
    //             ]);
    //         }
    //     }

    //     if ($apiSuccess) {
    //         $snapshot->pushed_at = Carbon::now()->toDateTimeString();
    //         $snapshot->push_destination = 'splunk';
    //         $snapshot->save();

    //         \Log::channel('metrics')->info('Snapshot pushed to Splunk successfully', [
    //             'category'  => 'metrics',
    //             'operation' => 'push',
    //             'result'    => 'success',
    //             'data'      => [
    //                 'snapshot_id' => $snapshot->id,
    //                 'destination' => 'splunk'
    //             ]
    //         ]);

    //         return true;
    //     }

    //     $snapshot->pushed_at = Carbon::now()->toDateTimeString();
    //     $snapshot->push_destination = 'splunk';
    //     $snapshot->error_data = 'One or more metric pushes encountered an error.';
    //     $snapshot->save();

    //     return false;
    // }
}
