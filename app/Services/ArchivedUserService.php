<?php

namespace App\Services;

use App\ArchivedUser;
use Uncgits\ZoomApi\Exceptions\ZoomApiException;

class ArchivedUserService
{
    public function archiveAll()
    {
        $users = \ZoomApi::using('users')
            ->listUsers();

        if ($users->status() !== 'success') {
            throw new ZoomApiException('API Error: ' . $users->lastCode() . ' - ' . $users->lastReason());
        }

        \DB::transaction(function () use ($users) {
            $users->content()->each(function ($user) {
                ArchivedUser::updateOrCreate(['id' => $user->id], (array) $user);
            });
        });
    }
}
