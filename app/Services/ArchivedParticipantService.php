<?php

namespace App\Services;

use App\ArchivedMeeting;
use App\ArchivedWebinar;
use App\Jobs\FetchMeetingParticipants;
use App\Jobs\FetchWebinarParticipants;

class ArchivedParticipantService
{
    public function archive()
    {
        $webinarsToCrunch = ArchivedWebinar::orderByDesc('start_time') // prioritize latest over backfills
            ->whereNull('participants_fetched_at')
            ->whereNull('participants_fetched_message')
            ->limit(config('zst.metrics.participant_batch_size'))
            ->get();

        $webinarsToCrunch->each(function ($webinar) {
            dispatch(new FetchWebinarParticipants($webinar));
        });

        // subtract number of webinars we just got. those should be far fewer.
        $meetingsToCrunch = ArchivedMeeting::orderByDesc('start_time') // prioritize latest over backfills
            ->whereNull('participants_fetched_at')
            ->whereNull('participants_fetched_message')
            ->limit(config('zst.metrics.participant_batch_size') - $webinarsToCrunch->count())
            ->get();

        $meetingsToCrunch->each(function ($meeting) {
            dispatch(new FetchMeetingParticipants($meeting));
        });
    }
}
