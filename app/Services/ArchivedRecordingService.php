<?php

namespace App\Services;

use Carbon\Carbon;
use App\ArchivedMeeting;
use App\ArchivedWebinar;
use App\ArchivedRecording;
use Uncgits\ZoomApi\Exceptions\ZoomApiException;

class ArchivedRecordingService
{
    public function archiveForDay(Carbon $day)
    {
        return $this->archiveForDateRange($day, $day->clone());
    }

    public function archiveForDateRange(Carbon $from, Carbon $to)
    {
        $params = [
            'from' => $from->format('Y-m-d'),
            'to'   => $to->format('Y-m-d'),
        ];

        $recordings = \ZoomApi::using('CloudRecording')
            ->addParameters($params)
            ->listRecordingsOfAccount();

        if ($recordings->status() !== 'success') {
            throw new ZoomApiException('API Error: ' . $recordings->lastCode() . ' - ' . $recordings->lastReason());
        }

        \DB::transaction(function () use ($recordings, $from, $to) {
            // scope it but add padding here "just in case"
            $meetingIds = ArchivedMeeting::whereBetween('start_time', [$from->subDays(2), $to->addDays(2)])->pluck('uuid');
            $webinarIds = ArchivedWebinar::whereBetween('start_time', [$from->subDays(2), $to->addDays(2)])->pluck('uuid');

            $recordings->content()->each(function ($recording) use ($meetingIds, $webinarIds) {
                $recordingFile = $recording->recording_files;
                foreach ($recordingFile as $file) {
                    $fileInfo = (array) $file;
                    if ($fileInfo['recording_end'] == '') {
                        $fileInfo['recording_end'] = null; // must be a valid datetime or null
                    }
                    if (isset($fileInfo['meeting_id'])) {
                        $fileInfo['attendable_id'] = $fileInfo['meeting_id'];
                        unset($fileInfo['meeting_id']);
                    }

                    if ($meetingIds->contains($fileInfo['attendable_id'])) {
                        $fileInfo['attendable_type'] = ArchivedMeeting::class;
                    } elseif ($webinarIds->contains($fileInfo['attendable_id'])) {
                        $fileInfo['attendable_type'] = ArchivedWebinar::class;
                    } else {
                        $fileInfo['attendable_type'] = null;
                    }


                    if (isset($fileInfo['id'])) {
                        $fileInfo['zoom_id'] = $fileInfo['id'];
                        unset($fileInfo['id']);
                        ArchivedRecording::updateOrCreate(['zoom_id' => $fileInfo['zoom_id']], $fileInfo);
                    } else {
                        ArchivedRecording::create($fileInfo);
                    }
                }
            });
        });

        // OLD INDIVIDUAL MEETINGS METHOD

        // $meetingsToCrunch = ArchivedMeeting::whereBetween('start_time', [$start, $end])
        //     ->whereNull('recordings_fetched_at')
        //     ->where('has_recording', true)
        //     ->get();

        // $meetingsToCrunch->each(function ($meeting) {
        //     dispatch(new FetchMeetingRecordings($meeting));
        // });
    }

    public function backfillMissingAttendableTypes()
    {
        $meetingService = new ArchivedMeetingService;

        $recordings = ArchivedRecording::whereNull('attendable_type')->get();
        $recordings->each(function ($recording) use ($meetingService) {
            $meeting = \ZoomApi::using('dashboards')
                ->addParameters(['type' => 'past'])
                ->getMeetingDetails(urlencode(urlencode($recording->attendable_id)));

            if ($meeting->lastCode() === 404) {
                $meeting = \ZoomApi::using('dashboards')
                    ->addParameters(['type' => 'pastOne'])
                    ->getMeetingDetails(urlencode(urlencode($recording->attendable_id)));
            }

            if ($meeting->status() === 'success') {
                $meetingService->archiveMeeting(json_decode($meeting->content()->toJson()));

                $recording->attendable_type = ArchivedMeeting::class;
                $recording->save();
            }
        });
    }
}
