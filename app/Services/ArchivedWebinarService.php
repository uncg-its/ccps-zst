<?php

namespace App\Services;

use Carbon\Carbon;
use App\ArchivedWebinar;
use App\Jobs\FetchWebinarReport;
use Uncgits\ZoomApi\Exceptions\ZoomApiException;

class ArchivedWebinarService
{
    public function archiveForDay(Carbon $day)
    {
        $this->archiveForDateRange($day, $day->clone());
    }

    private function fetchWebinarsFromApi($parameters)
    {
        return \ZoomApi::using('dashboards')
            ->addParameters($parameters)
            ->listWebinars();
    }

    public function archiveForDateRange(Carbon $from, Carbon $to)
    {
        $parameters = [
            'from' => $from->format('Y-m-d'),
            'to'   => $to->format('Y-m-d'),
        ];

        $currentTry = 0;
        $retryDelay = config('zst.api.rate_limit.backoff_seconds');
        $maxTries = count($retryDelay);

        $types = ['past'];

        foreach ($types as $type) {
            do {
                $shouldRetry = false;
                $currentTry++;
                $webinars = $this->fetchWebinarsFromApi(array_merge($parameters, ['type' => $type]));

                if ($webinars->status() !== 'success') {
                    if ($currentTry < $maxTries && $webinars->lastCode() === 429) {
                        $shouldRetry = true;
                        sleep($retryDelay[$currentTry - 1]);
                    }
                }
            } while ($shouldRetry);

            if ($webinars->status() !== 'success') {
                throw new ZoomApiException('API Error: ' . $webinars->lastCode() . ' - ' . $webinars->lastReason());
            }

            $webinars->content()->each(function ($webinar) {
                $this->archiveWebinar($webinar);
            });
        }
    }

    public function archiveWebinar($webinar)
    {
        $archivedWebinar = ArchivedWebinar::updateOrCreate(['uuid' => $webinar->uuid], (array) $webinar);
        dispatch(new FetchWebinarReport($webinar->uuid)) // don't know why we have to pass this, but...
            ->delay(now()->addSeconds(15));
    }
}
