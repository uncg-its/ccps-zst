<?php

namespace App\Services;

use Carbon\Carbon;
use App\ArchivedClientSatisfaction;
use Uncgits\ZoomApi\Exceptions\ZoomApiException;

class ArchivedClientSatisfactionService
{
    public function archiveForDay(Carbon $day)
    {
        $params = [
            'from' => $day->format('Y-m-d'),
            'to'   => $day->format('Y-m-d'),
        ];

        $satisfaction = \ZoomApi::using('dashboards')
            ->addParameters($params)
            ->listClientMeetingSatisfaction();

        if ($satisfaction->status() !== 'success') {
            throw new ZoomApiException('API Error: ' . $satisfaction->lastCode() . ' - ' . $satisfaction->lastReason());
        }

        // normally this is an array of objects. but we're asking for only one day's worth.
        $satisfactionRecords = collect($satisfaction->content()['client_satisfaction']);
        $recordForDay = $satisfactionRecords->first();

        ArchivedClientSatisfaction::updateOrCreate(['date' => $recordForDay->date], (array) $recordForDay);
    }
}
