<?php

namespace App\Services;

use Carbon\Carbon;
use App\ArchivedUser;
use App\ArchivedReport;
use App\ArchivedMeeting;
use App\ArchivedWebinar;
use App\ArchivedRecording;
use App\ArchivedParticipant;
use Uncgits\Ccps\MetricsSnapshots\Traits\GeneratesMetricsSnapshots;
use Uncgits\Ccps\MetricsSnapshots\Contracts\MetricsSnapshotGenerator;

class MetricsSnapshotService implements MetricsSnapshotGenerator
{
    use GeneratesMetricsSnapshots;

    public function calculateMetrics(Carbon $day): array
    {
        // REPORTS for day
        $reportsForDay = ArchivedReport::where('date', $day->format('Y-m-d'))->get();
        $cloudRecordingStorageReport = $reportsForDay->where('type', 'cloud_recording_storage')->first();
        $dailyUsageReport = $reportsForDay->where('type', 'daily_usage')->first();

        // USERS
        $activeUserCount = ArchivedUser::count();
        $newUsers = $dailyUsageReport->data['new_users'];


        // RECORDINGS
        $recordingStorageUsed = $cloudRecordingStorageReport->data['usage'];
        // parse into GB
        $recordingStorageParsed = explode(' ', $recordingStorageUsed);
        $recordingGbUsed = (float) $recordingStorageParsed[0]; // assume GB since that's the unit we want

        if ($recordingStorageParsed[1] === 'MB') {
            $recordingGbUsed = 1 / $recordingStorageParsed[0];
        } elseif ($recordingStorageParsed[1] === 'TB') {
            $recordingGbUsed = $recordingStorageParsed[0] * 1000; // or is it 1024?
        }

        // MEETINGS
        $meetingsOnDay = ArchivedMeeting::startedOnDay($day)
            ->select('uuid', 'start_time', 'end_time', 'has_pstn')
            ->get();

        $numberOfMeetingsForDay = $meetingsOnDay->count();

        $numberOfMeetingsWithRecording = ArchivedMeeting::startedOnDay($day)
            ->has('archived_recordings')
            ->count();

        // $meetingMinutes = $dailyUsageReport->data->meeting_minutes; // nope.
        $meetingMinutes = $meetingsOnDay->sum(function ($meeting) {
            return $meeting->start_time->diffInSeconds($meeting->end_time);
        }) / 60;

        // WEBINARS
        $webinarsOnDay = ArchivedWebinar::startedOnDay($day)
            ->select('uuid', 'start_time', 'end_time', 'has_pstn')
            ->get();

        $numberOfWebinarsForDay = $webinarsOnDay->count();

        $numberOfWebinarsWithRecording = ArchivedWebinar::startedOnDay($day)
            ->has('archived_recordings')
            ->count();

        // $webinarMinutes = $dailyUsageReport->data->webinar_minutes; // nope.
        $webinarMinutes = $webinarsOnDay->sum(function ($webinar) {
            return $webinar->start_time->diffInSeconds($webinar->end_time);
        }) / 60;

        // PARTICIPANTS
        $participantsOnDay = ArchivedParticipant::joinedOnDay($day)
            ->select('id', 'attendable_type', 'zoom_id', 'duration', 'user_name')
            ->get();

        $meetingParticipants = $participantsOnDay->where('attendable_type', ArchivedMeeting::class);
        $webinarParticipants = $participantsOnDay->where('attendable_type', ArchivedWebinar::class);

        // ASSEMBLE ARRAY

        // service_name => metric_name => metric_dimension format...

        $rawData = [
            'zoom' => [
                'licensing' => [
                    'meetings'   => $activeUserCount,
                    'webinars'   => 5, // TODO: how can we do this in a sane way? Basically, we can't. ListUsers doesn't give this - only GetUser. We'd have to loop thorugh all users to look at actual licensing, which is ludicrous.
                    'new_grants' => $newUsers,
                ],
                'storage'   => [
                    'used' => $recordingGbUsed
                ],
            ],
            'zoom.meetings' => [
                'connection_type' => [
                    'call_in'   => ArchivedParticipant::fromMeetings()->joinedOnDay($day)->calledIn()->count(),
                    'wifi'      => ArchivedParticipant::fromMeetings()->joinedOnDay($day)->onWifi()->count(),
                    'wired'     => ArchivedParticipant::fromMeetings()->joinedOnDay($day)->onWired()->count(),
                    'cell_data' => ArchivedParticipant::fromMeetings()->joinedOnDay($day)->onCellularData()->count(),
                ],
                'device_type'     => [
                    'mobile'    => ArchivedParticipant::fromMeetings()->joinedOnDay($day)->joinedFromMobile()->count(),
                    'chrome_os' => ArchivedParticipant::fromMeetings()->joinedOnDay($day)->joinedFromChromeOs()->count(),
                    'web_app'   => ArchivedParticipant::fromMeetings()->joinedOnDay($day)->joinedFromWeb()->count(),
                    'call_in'   => ArchivedParticipant::fromMeetings()->joinedOnDay($day)->calledIn()->count(),
                ],
                'participants'    => [
                    'total'         => $meetingParticipants->count(),
                    'unique'        => $meetingParticipants->unique('user_name')->count(),
                    'total_minutes' => round($meetingParticipants->sum('duration') / 60, 2),
                ],
                'recordings'      => [
                    'total' => ArchivedRecording::fromMeetings()->createdOnDay($day)->where('file_type', 'MP4')->count(),
                ],
                'sessions'        => [
                    'total'                 => $numberOfMeetingsForDay,
                    'recorded'              => $numberOfMeetingsWithRecording,
                    'with_telephony'        => $meetingsOnDay->where('has_pstn', true)->count(),
                    'total_minutes'         => round($meetingMinutes, 2),
                ],
            ],
            'zoom.webinars' => [
                'connection_type' => [
                    'call_in'   => ArchivedParticipant::fromWebinars()->joinedOnDay($day)->calledIn()->count(),
                    'wifi'      => ArchivedParticipant::fromWebinars()->joinedOnDay($day)->onWifi()->count(),
                    'wired'     => ArchivedParticipant::fromWebinars()->joinedOnDay($day)->onWired()->count(),
                    'cell_data' => ArchivedParticipant::fromWebinars()->joinedOnDay($day)->onCellularData()->count(),
                ],
                'device_type'     => [
                    'mobile'    => ArchivedParticipant::fromWebinars()->joinedOnDay($day)->joinedFromMobile()->count(),
                    'chrome_os' => ArchivedParticipant::fromWebinars()->joinedOnDay($day)->joinedFromChromeOs()->count(),
                    'web_app'   => ArchivedParticipant::fromWebinars()->joinedOnDay($day)->joinedFromWeb()->count(),
                    'call_in'   => ArchivedParticipant::fromWebinars()->joinedOnDay($day)->calledIn()->count(),
                ],
                'participants'    => [
                    'total'         => $webinarParticipants->count(),
                    'unique'        => $webinarParticipants->unique('user_name')->count(),
                    'total_minutes' => round($webinarParticipants->sum('duration') / 60, 2),
                ],
                'recordings'      => [
                    'total' => ArchivedRecording::fromWebinars()->createdOnDay($day)->where('file_type', 'MP4')->count(),
                ],
                'sessions'        => [
                    'total'                 => $numberOfWebinarsForDay,
                    'recorded'              => $numberOfWebinarsWithRecording,
                    'with_telephony'        => $webinarsOnDay->where('has_pstn', true)->count(),
                    'total_minutes'         => round($webinarMinutes, 2),
                ],
            ],
        ];

        // SESSION LENGTH CRUNCHING
        $rawData['zoom.meetings']['sessions'] = array_merge($rawData['zoom.meetings']['sessions'], $this->parseSessionLength($meetingsOnDay));
        $rawData['zoom.webinars']['sessions'] = array_merge($rawData['zoom.webinars']['sessions'], $this->parseSessionLength($webinarsOnDay));

        $formatted = [];
        foreach ($rawData as $service => $metric) {
            foreach ($metric as $metricName => $dimensions) {
                foreach ($dimensions as $dimensionName => $metricValue) {
                    $formatted[] = [
                        'service_name'     => $service,
                        'metric_name'      => $metricName,
                        'metric_dimension' => $dimensionName,
                        'metric_value'     => $metricValue,
                        'metric_timestamp' => $day->format(config('mch-pusher.timestamp_format'))
                    ];
                }
            }
        }

        return $formatted;
    }

    private function parseSessionLength($sessions)
    {
        $sessionLengthThresholds = config('zst.metrics.meeting_length_thresholds');

        $parsedSessions = [];

        foreach ($sessionLengthThresholds as $index => $limit) {
            $upperBoundIndex = $index + 1;

            if (!isset($sessionLengthThresholds[$upperBoundIndex])) {
                $upperBound = 99999;
                $upperBoundText = "+";
            } else {
                $upperBound = $sessionLengthThresholds[$upperBoundIndex];
                $upperBoundText = "-" . ($upperBound - 1);
            }

            $lengthKey = "length_" . $limit . $upperBoundText . '_minutes';

            if (!isset($parsedSessions[$lengthKey])) {
                $parsedSessions[$lengthKey] = 0;
            }

            // grab the count
            $parsedSessions[$lengthKey] = $sessions->filter(function ($session) use ($limit, $upperBound) {
                $duration = $session->start_time->diffInMinutes($session->end_time);
                return $duration >= $limit && $duration < $upperBound;
            })->count();
        }

        return $parsedSessions;
    }
}
