<?php

namespace App\Services;

use Carbon\Carbon;
use App\ArchivedReport;
use App\ArchivedMeeting;
use App\ArchivedParticipant;
use App\Jobs\FetchMeetingReport;
use App\Jobs\FetchParticipantReport;
use Carbon\CarbonPeriod;
use Uncgits\ZoomApi\Exceptions\ZoomApiException;

class ArchivedReportService
{
    public function archiveForDateRange(Carbon $from, Carbon $to)
    {
        if ($from->lte($to)) {
            $range = CarbonPeriod::create($from, $to);
        } else {
            $range = CarbonPeriod::create($to, $from);
        }

        foreach ($range as $date) {
            $this->archiveForDay($date);
        }
    }

    public function archiveForDay(Carbon $day)
    {
        $date = $day->format('Y-m-d');
        $month = $day->format('m');
        $year = $day->format('Y');

        // Daily Usage
        $params = [
            'year'  => $year,
            'month' => $month,
        ];

        $dailyUsageReport = \ZoomApi::using('reports')
            ->addParameters($params)
            ->getDailyUsageReport();

        if ($dailyUsageReport->status() !== 'success') {
            throw new ZoomApiException('API Error: ' . $dailyUsageReport->lastCode() . ' - ' . $dailyUsageReport->lastReason());
        }

        $reportForDay = collect($dailyUsageReport->content()['dates'])->where('date', $date)->first();
        ArchivedReport::updateOrCreate(['date' => $date, 'type' => 'daily_usage'], [
            'data' => $reportForDay
        ]);

        // Active / Inactive Host
        // SKIP for now. Not really sure this gives us anything useful.

        // Telephone Report
        // SKIP - we don't have toll-free audio

        // Cloud Recording Usage Report
        $to = $day->clone()->addDay()->format('Y-m-d'); // because for some reason asking only for one day doesn't work.

        $params = [
            'from' => $date,
            'to'   => $to
        ];

        $cloudRecordingUsageReport = \ZoomApi::using('reports')
            ->addParameters($params)
            ->getCloudRecordingUsageReport();

        if ($cloudRecordingUsageReport->status() !== 'success') {
            throw new ZoomApiException('API Error: ' . $cloudRecordingUsageReport->lastCode() . ' - ' . $cloudRecordingUsageReport->lastReason());
        }

        $reportForDay = collect($cloudRecordingUsageReport->content()['cloud_recording_storage'])->where('date', $date)->first();

        if (!is_null($reportForDay)) {
            ArchivedReport::updateOrCreate(['date' => $date, 'type' => 'cloud_recording_storage'], [
                'data' => $reportForDay
            ]);
        }


        // Meeting Detail? - would be mainly to flesh out the meeting data from the other API. - total minutes field

        // NOW DOING THIS ON THE MEETINGS JOB.

        // $meetingsToCrunch = ArchivedMeeting::whereNull('report_fetched_at')->get();
        // $meetingsToCrunch->each(function ($meeting) {
        //     dispatch(new FetchMeetingReport($meeting));
        // });

        // Meeting Participant - would be again good since we are missing some fields from other API. - email field

        // NOW DOING THIS ON THE PARTICIPANTS JOB.

        // $meetingsToCrunch = ArchivedParticipant::whereNull('user_email')->get()->groupBy('session_id');
        // if ($meetingsToCrunch->isNotEmpty()) {
        //     $meetingsToCrunch->each(function ($participants, $session_id) {
        //         $meeting = ArchivedMeeting::findOrFail($session_id);
        //         dispatch(new FetchParticipantReport($meeting, $participants));
        //     });
        // }
    }
}
