<?php

namespace App\Services;

use Carbon\Carbon;
use App\ArchivedMeeting;
use App\Jobs\FetchMeetingReport;
use Uncgits\ZoomApi\Exceptions\ZoomApiException;

class ArchivedMeetingService
{
    public function archiveForDay(Carbon $day)
    {
        $this->archiveForDateRange($day, $day->clone());
    }

    private function fetchMeetingsFromApi($parameters)
    {
        return \ZoomApi::using('dashboards')
            ->addParameters($parameters)
            ->listMeetings();
    }

    public function archiveForDateRange(Carbon $from, Carbon $to, $types = ['past', 'pastOne'])
    {
        $parameters = [
            'from' => $from->format('Y-m-d'),
            'to'   => $to->format('Y-m-d'),
        ];

        $currentTry = 0;
        $retryDelay = config('zst.api.rate_limit.backoff_seconds');
        $maxTries = count($retryDelay);

        foreach ($types as $type) {
            do {
                $shouldRetry = false;
                $currentTry++;
                $meetings = $this->fetchMeetingsFromApi(array_merge($parameters, ['type' => $type]));

                if ($meetings->status() !== 'success') {
                    if ($currentTry < $maxTries && $meetings->lastCode() === 429) {
                        $shouldRetry = true;
                        sleep($retryDelay[$currentTry - 1]);
                    }
                }
            } while ($shouldRetry);

            if ($meetings->status() !== 'success') {
                throw new ZoomApiException('API Error: ' . $meetings->lastCode() . ' - ' . $meetings->lastReason());
            }

            $meetings->content()->each(function ($meeting) {
                $this->archiveMeeting($meeting);
            });
        }
    }

    public function archiveMeeting($meeting)
    {
        $archivedMeeting = ArchivedMeeting::updateOrCreate(['uuid' => $meeting->uuid], (array) $meeting);
        dispatch(new FetchMeetingReport($meeting->uuid)) // don't know why we have to pass this, but...
            ->delay(now()->addSeconds(15));
    }
}
