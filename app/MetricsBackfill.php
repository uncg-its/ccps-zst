<?php

namespace App;

use App\CcpsCore\User;
use Illuminate\Database\Eloquent\Model;

class MetricsBackfill extends Model
{
    protected $guarded = [];

    protected $casts = [
        'from' => 'datetime',
        'to' => 'datetime'
    ];

    // relationships

    public function created_by()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function requester()
    {
        return $this->created_by();
    }
}
