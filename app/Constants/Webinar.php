<?php

namespace App\Constants;

class Webinar
{
    public static $type = [
        5 => 'Webinar',
        6 => 'Recurring webinar, no fixed time',
        9 => 'Recurring webinar, fixed time',
    ];

    public static $approval_type = [
        0 => 'Automatically approve',
        1 => 'Manually approve',
        2 => 'No regisration required'
    ];

    public static $registration_type = [
        1 => 'Register once, attend all occurrences',
        2 => 'Register for each occurrence',
        3 => 'Register once, choose occurrences to attend'
    ];

    public static $audio = [
        'both' => 'Telephony and VoIP',
        'telephony' => 'Telephony only',
        'voip' => 'VoIP only'
    ];

    public static function expandDetails($webinar)
    {
        $webinar['type'] = self::$type[$webinar['type']];
        $webinar['settings']->registration_type = isset($webinar['settings']->registration_type) ?
            self::$registration_type[$webinar['settings']->registration_type] :
            'none';
        $webinar['settings']->approval_type = isset($webinar['settings']->approval_type) ?
            self::$approval_type[$webinar['settings']->approval_type] :
            'none';
        $webinar['settings']->audio = self::$audio[$webinar['settings']->audio];

        return $webinar;
    }
}
