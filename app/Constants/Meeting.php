<?php

namespace App\Constants;

class Meeting
{
    public static $type = [
        1 => 'Instant meeting',
        2 => 'Scheduled meeting',
        3 => 'Recurring meeting, no fixed time',
        8 => 'Recurring meeting, fixed time',
    ];

    public static $approval_type = [
        0 => 'Automatically approve',
        1 => 'Manually approve',
        2 => 'No regisration required'
    ];

    public static $registration_type = [
        1 => 'Register once, attend all occurrences',
        2 => 'Register for each occurrence',
        3 => 'Register once, choose occurrences to attend'
    ];

    public static $audio = [
        'both' => 'Telephony and VoIP',
        'telephony' => 'Telephony only',
        'voip' => 'VoIP only'
    ];

    public static function expandDetails($meeting)
    {
        $meeting['type'] = self::$type[$meeting['type']];
        $meeting['settings']->registration_type = isset($meeting['settings']->registration_type) ?
            self::$registration_type[$meeting['settings']->registration_type] :
            'none';
        $meeting['settings']->approval_type = isset($meeting['settings']->approval_type) ?
            self::$approval_type[$meeting['settings']->approval_type] :
            'none';
        $meeting['settings']->audio = self::$audio[$meeting['settings']->audio];

        return $meeting;
    }
}
