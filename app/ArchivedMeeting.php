<?php

namespace App;

use App\Traits\ArchivableSession;
use App\Contracts\ArchivedSession;
use Illuminate\Database\Eloquent\Model;

class ArchivedMeeting extends Model implements ArchivedSession
{
    use ArchivableSession;

    protected $primaryKey = 'uuid';

    protected $casts = [
        'uuid'                    => 'string',
        'participants_fetched_at' => 'datetime',
        'recordings_fetched_at'   => 'datetime',
        'start_time'              => 'datetime',
        'end_time'                => 'datetime'
    ];

    protected $guarded = [];
}
