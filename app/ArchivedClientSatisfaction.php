<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArchivedClientSatisfaction extends Model
{
    protected $guarded = [];
}
