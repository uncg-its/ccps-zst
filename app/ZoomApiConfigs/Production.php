<?php

namespace App\ZoomApiConfigs;

use Uncgits\ZoomApi\ZoomApiConfig;

class Production extends ZoomApiConfig
{
    public function __construct()
    {
        $config = config('zoom-api.configs.production');
        $this->setApiKey($config['key']);
        $this->setApiSecret($config['secret']);

        $this->setUseProxy($config['proxy']['use']);
        if ($config['proxy']['use']) {
            $this->setProxyHost($config['proxy']['host']);
            $this->setProxyPort($config['proxy']['port']);
        }
    }
}
