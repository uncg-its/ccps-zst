<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ArchivedRecording extends Model
{
    protected $primaryKey = 'id';

    protected $guarded = [];

    protected $casts = [
        'recording_start' => 'datetime',
        'recording_end'   => 'datetime'
    ];

    //relationships

    public function attendable()
    {
        return $this->morphTo('attendable');
    }

    // aliases

    public function session()
    {
        return $this->attendable();
    }

    // scopes

    public function scopeFromMeetings($query)
    {
        return $query->where('attendable_type', ArchivedMeeting::class);
    }

    public function scopeFromWebinars($query)
    {
        return $query->where('attendable_type', ArchivedWebinar::class);
    }

    public function scopeCreatedOnDay($query, $day)
    {
        if (!is_a($day, Carbon::class)) {
            $day = Carbon::parse($day);
        }

        return $query->whereBetween('recording_start', [$day->startOfDay(), $day->clone()->endOfDay()]);
    }
}
