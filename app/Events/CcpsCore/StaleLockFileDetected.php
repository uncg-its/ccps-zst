<?php

namespace App\Events\CcpsCore;

use Uncgits\Ccps\Traits\NotifiesChannels;
use Uncgits\Ccps\Events\StaleLockFileDetected as BaseEvent;

class StaleLockFileDetected extends BaseEvent
{
    // even though parent event uses this trait, re-use it here so that `self::class`
    // returns this class name instead of the parent's.
    use NotifiesChannels;
}
