<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ArchivedParticipant extends Model
{
    protected $casts = [
        'join_time'  => 'datetime',
        'leave_time' => 'datetime'
    ];

    protected $guarded = [];

    // relationships

    public function attendable()
    {
        return $this->morphTo();
    }

    // aliases

    public function session()
    {
        return $this->attendable();
    }

    // scopes

    public function scopeFromMeetings($query)
    {
        return $query->where('attendable_type', ArchivedMeeting::class);
    }

    public function scopeFromWebinars($query)
    {
        return $query->where('attendable_type', ArchivedWebinar::class);
    }

    public function scopeJoinedOnDay($query, $day)
    {
        if (!is_a($day, Carbon::class)) {
            $day = Carbon::parse($day);
        }

        return $query->whereBetween('join_time', [$day->startOfDay(), $day->clone()->endOfDay()]);
    }

    public function scopeJoinedFromMobile($query)
    {
        return $query->where('device', 'LIKE', '%iOS%')
            ->orWhere('device', 'LIKE', '%iPAD%')
            ->orWhere('device', 'LIKE', '%Android%')
            ->orWhere('device', 'LIKE', '%Mobile%');
    }

    public function scopeJoinedFromChromeOs($query)
    {
        return $query->where('device', 'LIKE', '%Chrome%');
    }

    public function scopeJoinedFromWeb($query)
    {
        return $query->where('device', 'LIKE', '%Web%');
    }

    public function scopeCalledIn($query)
    {
        return $query->where('device', 'LIKE', '%Phone%');
    }

    public function scopeOnWifi($query)
    {
        return $query->where('network_type', 'LIKE', '%Wifi%');
    }

    public function scopeOnWired($query)
    {
        return $query->where('network_type', 'LIKE', '%Wired%');
    }

    public function scopeOnCellularData($query)
    {
        return $query->where('network_type', 'LIKE', '%3G%')
            ->orWhere('network_type', 'LIKE', '%PPP%')
            ->orWhere('network_type', 'LIKE', '%Others%');
    }
}
