<?php

namespace App\CcpsCore;

use App\MetricsBackfill;
use Uncgits\Ccps\Models\User as BaseModel;

class User extends BaseModel
{
    // relationships
    public function metrics_backfills()
    {
        return $this->hasMany(MetricsBackfill::class, 'user_id');
    }
}
