<?php

namespace App\Http\Controllers\Auth;

use Uncgits\Ccps\Controllers\Auth\RegisterController as BaseController;

class RegisterController extends BaseController
{
    public function __construct() {
        parent::__construct();
    }
}
