<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

class UserController extends Controller
{
    private function parseUserEmail($query)
    {
        if (strpos($query, '@' . config('zst.domain')) === false) {
            $query .= '@' . config('zst.domain');
        }

        return $query;
    }

    public function show($query)
    {
        $query = $this->parseUserEmail($query);

        $result = \ZoomApi::using('users')
            ->getUser($query);

        if ($result->status() !== 'success') {
            return response()->json(['error' => $result->lastReason()], $result->lastCode());
        }

        return response()->json(['query' => $query, 'user' => $result->getContent()], 200);
    }

    public function settings($query)
    {
        $query = $this->parseUserEmail($query);

        $result = \ZoomApi::using('users')
            ->getUserSettings($query);

        if ($result->status() !== 'success') {
            return response()->json(['error' => $result->lastReason()], $result->lastCode());
        }

        return response()->json(['query' => $query, 'settings' => $result->getContent()], 200);
    }

    public function meetings($query)
    {
        $query = $this->parseUserEmail($query);

        $result = \ZoomApi::using('meetings')
            ->listMeetings($query);

        if ($result->status() !== 'success') {
            return response()->json(['error' => $result->lastReason()], $result->lastCode());
        }

        return response()->json(['query' => $query, 'meetings' => $result->getContent()], 200);
    }

    public function recordings($query)
    {
        $query = $this->parseUserEmail($query);

        $result = \ZoomApi::using('CloudRecording')
            ->addParameters([
                'from' => now()->subMonths(1)->format('Y-m-d'),
                'to'   => now()->format('Y-m-d')
            ])->listAllRecordings($query);

        if ($result->status() !== 'success') {
            return response()->json(['error' => $result->lastReason()], $result->lastCode());
        }

        return response()->json(['query' => $query, 'recordings' => $result->getContent()], 200);
    }
}
