<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Uncgits\Ccps\Support\CcpsPaginator;

class ScheduledWebinarController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:api.view']);
    }

    public function index(Request $request)
    {
        if ($request->has('email')) {
            // make call
            $webinarsResult = \ZoomApi::using('webinars')
                ->addParameters(['type' => 'upcoming'])
                ->listWebinars($request->email);

            if ($webinarsResult->lastCode() === 404) {
                flash('The given user was not found in Zoom.')->warning();
                return view('live.scheduled-webinars.index');
            }

            if ($webinarsResult->lastCode() === 400) {
                flash('The given user does not have Webinar privilege in Zoom.')->warning();
                return view('live.scheduled-webinars.index');
            }

            if ($webinarsResult->lastCode() !== 200) {
                flash('API Error while making request. Please contact an administrator.')->error();
                return view('live.scheduled-webinars.index');
            }

            $webinars = $webinarsResult->content();

            return view('live.scheduled-webinars.index')->with([
                'webinars' => new CcpsPaginator($webinars)
            ]);
        }

        return view('live.scheduled-webinars.index');
    }

    public function show(Request $request, $id)
    {
        $webinarResult = \ZoomApi::using('webinars')
            ->getWebinar($id);

        if ($webinarResult->lastCode() === 404) {
            flash('The given webinar was not found in Zoom.')->warning();
            return redirect()->back();
        }

        if ($webinarResult->lastCode() !== 200) {
            flash('API Error while making request. Please contact an administrator.')->error();
            return redirect()->back();
        }

        $webinar = \Webinar::expandDetails($webinarResult->content());

        return view('live.scheduled-webinars.show')->with([
            'webinar' => $webinar
        ]);
    }
}
