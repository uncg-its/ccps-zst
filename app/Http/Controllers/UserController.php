<?php

namespace App\Http\Controllers;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:api.view']);
    }

    public function index()
    {
        return view('live.users.index');
    }
}
