<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Uncgits\Ccps\Support\CcpsPaginator;

class ScheduledMeetingController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:api.view']);
    }

    public function index(Request $request)
    {
        if ($request->has('email')) {
            // make call
            $meetingsResult = \ZoomApi::using('meetings')
                ->addParameters(['type' => 'upcoming'])
                ->listMeetings($request->email);

            if ($meetingsResult->lastCode() === 404) {
                flash('The given user was not found in Zoom.')->warning();
                return view('live.scheduled-meetings.index');
            }

            if ($meetingsResult->lastCode() !== 200) {
                flash('API Error while making request. Please contact an administrator.')->error();
                return view('live.scheduled-meetings.index');
            }

            $meetings = $meetingsResult->content();

            return view('live.scheduled-meetings.index')->with([
                'meetings' => new CcpsPaginator($meetings)
            ]);
        }

        return view('live.scheduled-meetings.index');
    }

    public function show(Request $request, $id)
    {
        $meetingResult = \ZoomApi::using('meetings')
            ->getMeeting($id);

        if ($meetingResult->lastCode() === 404) {
            flash('The given meeting was not found in Zoom.')->warning();
            return redirect()->back();
        }

        if ($meetingResult->lastCode() !== 200) {
            flash('API Error while making request. Please contact an administrator.')->error();
            return redirect()->back();
        }

        $meeting = \Meeting::expandDetails($meetingResult->content());

        return view('live.scheduled-meetings.show')->with([
            'meeting' => $meeting
        ]);
    }
}
