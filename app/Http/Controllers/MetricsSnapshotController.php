<?php

namespace App\Http\Controllers;

use App\MetricsSnapshot;
use Illuminate\Http\Request;
use App\Services\MetricsSnapshotService;
use App\Repositories\MetricsSnapshotRepository;
use App\Http\Requests\PushMetricsSnapshotRequest;
use App\Http\Requests\CreateMetricsSnapshotRequest;
use App\Http\Requests\DeleteMetricsSnapshotRequest;
use App\Http\Requests\RestoreMetricsSnapshotRequest;

class MetricsSnapshotController extends Controller
{
    protected $repository;
    protected $service;

    public function __construct()
    {
        $this->middleware('permission:metrics.view')->only(['index', 'show']);
        $this->middleware('permission:metrics.create')->only(['create', 'store']);
        $this->middleware('permission:metrics.delete')->only(['destroy']);

        $this->repository = new MetricsSnapshotRepository;
        $this->service = new MetricsSnapshotService;
    }

    /**
     * Display a listing of the resource.
     *()
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $metricsSnapshots = $this->repository->paginated()->sorted('id', 'desc');

        if (\Auth::user()->hasRole('admin')) {
            $metricsSnapshots = $metricsSnapshots->withTrashed();
        }

        if ($request->anyFilled(array_keys($this->repository->searchable))) {
            $metricsSnapshots = $metricsSnapshots->search($request->all());
        } else {
            $metricsSnapshots = $metricsSnapshots->all();
        }

        return view('metrics.snapshots.index')->with([
            'metricsSnapshots' => $metricsSnapshots,
            'repository'       => $this->repository,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('metrics.snapshots.create')->with([
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateMetricsSnapshotRequest $request)
    {
        try {
            $this->service->generateForDay($request->date);
            flash('Snapshot generated successfully.')->success();
        } catch (\Throwable $th) {
            \Log::channel('metrics')->error('Error generating metrics snapshot', [
                'category'  => 'metrics',
                'operation' => 'snapshot',
                'result'    => 'failure',
                'data'      => [
                    'message' => $th->getMessage(),
                    'request' => $request->except('_token'),
                ]
            ]);
            flash($th->getMessage())->error();
            throw $th;
        }
        return redirect()->route('metrics.snapshots.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MetricsSnapshot  $metricsSnapshot
     * @return \Illuminate\Http\Response
     */
    public function show(MetricsSnapshot $metricsSnapshot)
    {
        return view('metrics.snapshots.show')->with([
            'metricsSnapshot' => $metricsSnapshot
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MetricsSnapshot  $metricsSnapshot
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeleteMetricsSnapshotRequest $request, MetricsSnapshot $metricsSnapshot)
    {
        $this->repository->delete($metricsSnapshot->id);
        flash('Snapshot deleted succcessfully')->success();
        return redirect()->route('metrics.snapshots.index');
    }

    public function restore(RestoreMetricsSnapshotRequest $request, $metricsSnapshot)
    {
        $metricsSnapshot = $this->repository
            ->withTrashed()
            ->get($metricsSnapshot);

        $this->repository = new MetricsSnapshotRepository; // reset

        $snapshotsWithSameAccountAndDate = $this->repository->search([
            'date'       => $metricsSnapshot->date,
            'account_id' => $metricsSnapshot->account_id,
        ]);

        if ($snapshotsWithSameAccountAndDate->isNotEmpty()) {
            flash('Cannot restore this snapshot - another already exists for this account ID and date.')->error();
        } else {
            $this->repository->restore($metricsSnapshot->id);
            flash('Snapshot restored successfully')->success();
        }

        return redirect()->route('metrics.snapshots.index');
    }

    public function push(PushMetricsSnapshotRequest $request, MetricsSnapshot $metricsSnapshot)
    {
        try {
            foreach ($metricsSnapshot->push_destination as $destination) {
                $method = 'pushTo' . \Str::studly($destination);

                if (!method_exists($this->service, $method)) {
                    $error = 'Unsupported metrics destination given in application configuration. Cannot proceed with push.';
                    \Log::channel('metrics')->warning($error, [
                        'category'  => 'metrics',
                        'operation' => 'push',
                        'result'    => 'failure',
                        'data'      => [
                            'destination' => $metricsSnapshot->push_destination
                        ]
                    ]);

                    throw new \Exception($error);
                }

                $this->service->$method($metricsSnapshot);
            }

            flash('Snapshot pushed successfully.')->success();
        } catch (\Throwable $th) {
            \Log::channel('metrics')->error('Error pushing metrics snapshot', [
                'category'  => 'metrics',
                'operation' => 'push',
                'result'    => 'error',
                'data'      => [
                    'message'     => $th->getMessage(),
                    'destination' => $metricsSnapshot->push_destination
                ]
            ]);

            flash('Error pushing snapshot. Please check logs.')->error();
        }

        return redirect()->back();
    }
}
