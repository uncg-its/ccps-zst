<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateMetricsBackfillRequest;
use App\MetricsBackfill;
use App\Services\ArchivedMeetingService;
use App\Services\ArchivedRecordingService;
use App\Services\ArchivedReportService;
use App\Services\ArchivedWebinarService;

class MetricsBackfillController extends Controller
{
    public function index()
    {
        return view('metrics.backfills.index')->with([
            'metricsBackfills' => MetricsBackfill::orderByDesc('created_at')->paginate()
        ]);
    }

    public function create()
    {
        return view('metrics.backfills.create');
    }

    public function store(CreateMetricsBackfillRequest $request)
    {
        try {
            $backfill = MetricsBackfill::create([
                'user_id' => \Auth::user()->id,
                'from'    => $request->from,
                'to'      => $request->to,
                'status'  => 'in-progress',
            ]);

            // get meetings for each day in the range
            $service = new ArchivedMeetingService;
            $service->archiveForDateRange($backfill->from, $backfill->to);

            // get webinars for each day in the range
            $service = new ArchivedWebinarService;
            $service->archiveForDateRange($backfill->from, $backfill->to);

            // get reports for each day in the range
            $service = new ArchivedReportService;
            $service->archiveForDateRange($backfill->from, $backfill->to);

            // get recordings for each meeting
            $service = new ArchivedRecordingService;
            $service->archiveForDateRange($backfill->from, $backfill->to);

            // participants will be picked up by the backfill cron and staggered for rate limiting purposes.

            flash('Backfill request created successfully.')->success();
        } catch (\Throwable $th) {
            flash('Error creating backfill request.')->error();
            throw $th;
        }

        return redirect()->route('metrics.backfills.index');
    }
}
