<?php

namespace App\Http\Controllers\CcpsCore;

use Laravel\Horizon\Horizon;
use Uncgits\Ccps\Controllers\HomeController as BaseController;

class HomeController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function horizon()
    {
        $horizonScriptVariables = Horizon::scriptVariables();
        $horizonScriptVariables['path'] = config('horizon.subfolder') . $horizonScriptVariables['path'];

        return view('horizon::layout', [
            'cssFile'                => Horizon::$useDarkTheme ? 'app-dark.css' : 'app.css',
            'horizonScriptVariables' => $horizonScriptVariables,
            'assetsAreCurrent'       => Horizon::assetsAreCurrent(),
            'isDownForMaintenance'   => \App::isDownForMaintenance(),
        ]);
    }
}
