<?php

namespace App\Http\Controllers\CcpsCore;

use Uncgits\Ccps\Controllers\MappedRoleGroupController as BaseController;

class MappedRoleGroupController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }
}
