<?php

namespace App\Http\Controllers\CcpsCore;

use Uncgits\Ccps\Controllers\NotificationLogController as BaseController;

class NotificationLogController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }
}
