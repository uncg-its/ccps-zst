<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateMetricsBackfillRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::user()->hasPermission('metrics.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'from' => 'required|date_format:Y-m-d',
            'to'   => 'required|date_format:Y-m-d|after_or_equal:from|before:' . now()->subDays(config('zst.metrics.days_ago'))->format('Y-m-d')
        ];
    }

    public function messages()
    {
        return [
            'to.after_or_equal' => 'The \'to\' date must be after the \'from\' date',
            'to.before' => 'You must select a \'to\' date that is before the date of the next metrics run'
        ];
    }
}
