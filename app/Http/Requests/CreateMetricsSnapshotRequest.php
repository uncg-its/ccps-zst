<?php

namespace App\Http\Requests;

use App\MetricsSnapshot;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class CreateMetricsSnapshotRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::user()->can('metrics.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => [
                'required',
                'date_format:Y-m-d',
                'before:today',
                Rule::notIn(MetricsSnapshot::withoutTrashed()->pluck('date'))
                // Rule::unique('metrics_snapshots', 'date')->ignore(MetricsSnapshot::onlyTrashed()->get())
            ]
        ];
    }

    public function messages()
    {
        return [
            'date.required' => 'The date field is required.',
            'date.date_format' => 'The date must be in YYYY-MM-DD format.',
            'date.before' => 'You must choose a date before today.',
            'date.not_in' => 'There is already a snapshot for this day.'
        ];
    }
}
