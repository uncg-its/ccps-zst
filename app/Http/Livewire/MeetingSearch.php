<?php

namespace App\Http\Livewire;

use Livewire\Component;

class MeetingSearch extends Component
{
    public $meetingNumber = '';
    public $result = '';

    protected $zoomMeeting = false; // complex data type, so can't be public

    public function updatedMeetingNumber()
    {
        $this->result = '';

        $meetingNumber = str_replace(' ', '', $this->meetingNumber);

        $meetingResult = \ZoomApi::using('meetings')
            ->getMeeting($meetingNumber);

        $code = $meetingResult->lastCode();

        if ($code === 404) {
            $this->result = 'not-found';
            return;
        }
        if ($code !== 200) {
            $this->result = 'error';
            return;
        }

        $this->zoomMeeting = $meetingResult->getContent();
    }

    public function render()
    {
        return view('livewire.meeting-search', ['zoomMeeting' => $this->zoomMeeting]);
    }
}
