<?php

namespace App\Http\Livewire;

use Livewire\Component;

class UserLookup extends Component
{
    public $username = '';
    public $result = '';

    protected $zoomUser = false; // complex data type, so can't be public

    public function updatedUsername()
    {
        $this->result = '';

        $username = (strpos($this->username, '@uncg.edu') === false) ?
            $this->username . '@uncg.edu' :
            $this->username;

        $userResult = \ZoomApi::using('users')
            ->getUser($username);

        $code = $userResult->lastCode();

        if ($code === 404) {
            $this->result = 'not-found';
            return;
        }
        if ($code !== 200) {
            $this->result = 'error';
            return;
        }

        $this->zoomUser = $userResult->getContent();
    }

    public function render()
    {
        return view('livewire.user-lookup', ['zoomUser' => $this->zoomUser]);
    }
}
