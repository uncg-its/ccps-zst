<?php

namespace App\Cronjobs;

use Uncgits\Ccps\Models\Cronjob;
use Uncgits\Ccps\Helpers\CronjobResult;
use Uncgits\ZoomApi\Exceptions\ZoomApiException;
use App\Services\ArchivedClientSatisfactionService;

class ArchiveClientSatisfaction extends Cronjob
{
    protected $schedule = '10 1 * * *'; // default schedule (overridable in database)
    protected $display_name = '[Metrics] Archive Client Satisfaction'; // default display name (overridable in database)
    protected $description = 'Archives Client Satisfaction survey results from X days ago'; // default description name (overridable in database)
    protected $tags = ['metrics']; // default tags (overridable in database)

    protected function execute()
    {
        try {
            $daysAgo = config('zst.metrics.days_ago');
            $service = new ArchivedClientSatisfactionService;
            $service->archiveForDay(now()->subDays($daysAgo));
        } catch (ZoomApiException $e) {
            return new CronjobResult(false, $e->getMessage());
        }

        return new CronjobResult(true);
    }
}
