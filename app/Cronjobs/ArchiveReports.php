<?php

namespace App\Cronjobs;

use App\Services\ArchivedReportService;
use Uncgits\Ccps\Helpers\CronjobResult;
use Uncgits\Ccps\Models\Cronjob;
use Uncgits\ZoomApi\Exceptions\ZoomApiException;

class ArchiveReports extends Cronjob
{
    protected $schedule = '0 22 * * *'; // default schedule (overridable in database)
    protected $display_name = '[Metrics] Archive Reports'; // default display name (overridable in database)
    protected $description = 'Archives reports from X days in the past, including augmenting meeting and participant data'; // default description name (overridable in database)
    protected $tags = ['metrics']; // default tags (overridable in database)

    protected function execute()
    {
        try {
            $daysAgo = config('zst.metrics.days_ago');

            $service = new ArchivedReportService;
            $service->archiveForDay(now()->subDays($daysAgo));
        } catch (ZoomApiException $e) {
            return new CronjobResult(false, $e->getMessage());
        }

        return new CronjobResult(true);
    }
}
