<?php

namespace App\Cronjobs;

use Carbon\Carbon;
use App\MetricsSnapshot;
use Uncgits\Ccps\Models\Cronjob;
use App\Services\MetricsSnapshotService;
use Uncgits\Ccps\Helpers\CronjobResult;

class GenerateDailyMetricsSnapshot extends Cronjob
{
    protected $schedule = '30 0 * * *'; // default schedule (overridable in database)
    protected $display_name = '[Metrics] Generate Daily Snapshot'; // default display name (overridable in database)
    protected $description = 'Generates a metrics snapshot for a given day (in the past, to ensure all reporting has been caught up).'; // default description name (overridable in database)
    protected $tags = ['metrics']; // default tags (overridable in database)

    protected function execute()
    {
        try {
            // add one because we're executing this job at 1am
            $day = Carbon::now()->subDays(config('zst.metrics.days_ago') + 1)->format('Y-m-d');
            \Log::channel('metrics')->info('Starting metrics generation for ' . $day, [
                'category'  => 'metrics',
                'operation' => 'generate',
                'result'    => 'starting',
                'data'      => [
                    'day' => $day,
                ]
            ]);

            // does a snapshot already exist for that day?
            if (MetricsSnapshot::where('date', $day)->count() > 0) {
                \Log::channel('metrics')->warning('Metrics already exist for this day', [
                    'category'  => 'metrics',
                    'operation' => 'generate',
                    'result'    => 'skipped',
                    'data'      => [
                        'day' => $day
                    ]
                ]);
                return;
            }

            $snapshot = (new MetricsSnapshotService)->generateForDay($day);

            \Log::channel('metrics')->info('Metrics snapshot generated', [
                'category'  => 'metrics',
                'operation' => 'generate',
                'result'    => 'success',
                'data'      => [
                    'day'         => $day,
                    'snapshot_id' => $snapshot->id
                ]
            ]);
        } catch (\Throwable $th) {
            \Log::channel('metrics')->error('Error while generating metrics snapshot', [
                'category'  => 'metrics',
                'operation' => 'generate',
                'result'    => 'error',
                'data'      => [
                    'day'     => $day,
                    'message' => $th->getMessage()
                ]
            ]);

            throw $th;
        }

        return new CronjobResult(true);
    }
}
