<?php

namespace App\Cronjobs;

use App\Services\ArchivedUserService;
use Uncgits\Ccps\Helpers\CronjobResult;
use Uncgits\Ccps\Models\Cronjob;
use Uncgits\ZoomApi\Exceptions\ZoomApiException;

class ArchiveUsers extends Cronjob
{
    protected $schedule = '30 1 * * *'; // default schedule (overridable in database)
    protected $display_name = '[Metrics] Archive Users'; // default display name (overridable in database)
    protected $description = 'Archives all site users to local database'; // default description name (overridable in database)
    protected $tags = ['metrics']; // default tags (overridable in database)

    protected function execute()
    {
        try {
            $service = new ArchivedUserService;
            $service->archiveAll();
        } catch (ZoomApiException $e) {
            return new CronjobResult(false, $e->getMessage());
        }

        return new CronjobResult(true);
    }
}
