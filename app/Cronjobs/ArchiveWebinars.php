<?php

namespace App\Cronjobs;

use Uncgits\Ccps\Models\Cronjob;
use Uncgits\Ccps\Helpers\CronjobResult;
use App\Services\ArchivedWebinarService;

class ArchiveWebinars extends Cronjob
{
    protected $schedule = '5 1 * * *'; // default schedule (overridable in database)
    protected $display_name = '[Metrics] Archive Webinars'; // default display name (overridable in database)
    protected $description = 'Archives webinars X days in the past (according to config)'; // default description name (overridable in database)
    protected $tags = ['metrics']; // default tags (overridable in database)

    protected function execute()
    {
        try {
            $daysAgo = config('zst.metrics.days_ago');

            $service = new ArchivedWebinarService;
            $service->archiveForDay(now()->subDays($daysAgo));
        } catch (ZoomApiException $e) {
            return new CronjobResult(false, $e->getMessage());
        }

        return new CronjobResult(true);
    }
}
