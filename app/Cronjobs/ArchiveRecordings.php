<?php

namespace App\Cronjobs;

use Uncgits\Ccps\Models\Cronjob;
use Uncgits\Ccps\Helpers\CronjobResult;
use App\Services\ArchivedRecordingService;
use Uncgits\ZoomApi\Exceptions\ZoomApiException;

class ArchiveRecordings extends Cronjob
{
    protected $schedule = '15 1 * * *'; // default schedule (overridable in database)
    protected $display_name = '[Metrics] Archive Recordings'; // default display name (overridable in database)
    protected $description = 'Archives recordings from meetings X days in the past'; // default description name (overridable in database)
    protected $tags = ['metrics']; // default tags (overridable in database)

    protected function execute()
    {
        try {
            $daysAgo = config('zst.metrics.days_ago');
            $service = new ArchivedRecordingService;
            $service->archiveForDay(now()->subDays($daysAgo));
        } catch (ZoomApiException $e) {
            return new CronjobResult(false, $e->getMessage());
        }

        return new CronjobResult(true);
    }
}
