<?php

namespace App\Cronjobs;

use Uncgits\Ccps\Models\Cronjob;
use Uncgits\Ccps\Helpers\CronjobResult;
use App\Services\ArchivedParticipantService;

class ArchiveParticipants extends Cronjob
{
    protected $schedule = '*/2 8-21 * * *'; // default schedule (overridable in database)
    protected $display_name = '[Metrics] Archive Participants'; // default display name (overridable in database)
    protected $description = 'Archives meeting participants gradually, to adhere to API rate limits. Prioritizes newest meetings over backfills.'; // default description name (overridable in database)
    protected $tags = ['metrics']; // default tags (overridable in database)

    protected function execute()
    {
        $service = new ArchivedParticipantService;
        $service->archive(); // just grab them from the database, they're prioritized newest-first. this will pick up backfills too.

        return new CronjobResult(true);
    }
}
