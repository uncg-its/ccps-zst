<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArchivedReport extends Model
{
    protected $guarded = [];

    protected $casts = [
        'data' => 'json'
    ];
}
