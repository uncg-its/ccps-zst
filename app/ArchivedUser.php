<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArchivedUser extends Model
{
    public $incrementing = false;

    public $timestamps = false;

    protected $casts = [
        'created_at'      => 'datetime',
        'last_login_time' => 'datetime'
    ];

    protected $guarded = [];
}
