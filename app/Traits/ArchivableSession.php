<?php

namespace App\Traits;

use Carbon\Carbon;
use App\ArchivedRecording;
use App\ArchivedParticipant;

trait ArchivableSession
{
    public function getEncodedUuidAttribute()
    {
        return urlencode(urlencode($this->uuid));
    }

    // relationships

    public function archived_participants()
    {
        return $this->morphMany(ArchivedParticipant::class, 'attendable');
    }

    public function archived_recordings()
    {
        return $this->morphMany(ArchivedRecording::class, 'attendable');
    }

    // scopes

    public function scopeStartedOnDay($query, $day)
    {
        if (!is_a($day, Carbon::class)) {
            $day = Carbon::parse($day);
        }

        return $query->whereBetween('start_time', [$day->startOfDay(), $day->clone()->endOfDay()]);
    }
}
