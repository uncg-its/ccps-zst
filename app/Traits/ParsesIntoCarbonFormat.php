<?php

namespace App\Traits;

use Carbon\Carbon;

trait ParsesIntoCarbonFormat
{
    protected function parseIntoCarbon($day)
    {
        if (is_a($day, Carbon::class)) {
            return $day;
        }

        try {
            $day = Carbon::parse($day);
        } catch (\Exception $e) {
            // could not parse.

            // if it's a timestamp format in a string, convert to int
            if (preg_match('/[0-9]{10}/', $day) == 0) {
                throw $e;
            }
            $day = Carbon::createFromTimestamp((int) $day);
        }

        return $day;
    }
}
