<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Uncgits\Ccps\MetricsSnapshots\Contracts\Pushable;
use Uncgits\Ccps\MetricsSnapshots\MetricPush;

class MetricsSnapshot extends Model implements Pushable
{
    use SoftDeletes;

    public $timestamps = false;
    protected $dates = [
        'created_at',
        'verified_at',
        'pushed_at'
    ];

    protected $guarded = [];

    protected $casts = [
        'raw_data'         => 'array',
        'push_destination' => 'json'
    ];

    // relationships
    public function metric_pushes()
    {
        return $this->hasMany(MetricPush::class);
    }

    // metrics pushing
    public static function getMetricDataField(): string
    {
        return 'raw_data';
    }

    // accessors

    public function getPushedAttribute()
    {
        return ! is_null($this->pushed_at);
    }
}
