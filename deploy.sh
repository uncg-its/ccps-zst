# this is a POC deploy script that will assume you're inside the Docker container already, and in the app folder.


# maintenance mode
php artisan down

# get latest from git
git pull origin master
# password prompt here unless you're using SSH

# composer
composer install --no-interaction --prefer-dist --optimize-autoloader --no-dev
# composer install --no-interaction --prefer-dist --optimize-autoloader

# migrations
php artisan migrate --force

# clear caches
php artisan cache:clear

# clear / rebuild route cache
php artisan route:clear
# php artisan route:cache

# clear / rebuild config cache
# php artisan config:clear
php artisan config:cache

# clear / rebuild view cache
php artisan view:clear
# php artisan view:cache

# build npm if needed
# npm ci

# build assets with laravel mix
# npm run production

# restart queue
supervisorctl restart ccps-zst-horizon

# back in business
php artisan up
