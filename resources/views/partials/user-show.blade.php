@if($context == 'profile')
    <h1>My Profile</h1>
@else
    <h1>User - {{ $userToShow->email }}</h1>
@endif

<div class="row mt-3">
    <div class="col">
        <div class="card mb-3">
            <div class="card-header">
                <h5 class="mb-0">User Information</h5>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <p><strong>Email</strong>: {{ $userToShow->email }}</p>
                        <p><strong>Provider</strong>: <i
                                    class="{{ $userToShow->provider_icon ?? 'fas fa-address-book' }} mr-1"
                                    title="Provider: {{ ucwords($userToShow->provider) }}"></i> {{ ucwords($userToShow->provider) }}
                        </p>
                        @if($userToShow->provider != 'local')
                            <p><strong>Provider ID</strong>: {{ $userToShow->id_from_provider }}</p>
                        @endif
                        <p><strong>Time Zone</strong>: {{ $userToShow->time_zone }}</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <h5 class="mb-0">Actions</h5>
            </div>
            <div class="card-body">
                <div class="d-flex flex-column flex-md-row">
                    @if($context == 'profile')
                        <a href="{{ route('profile.edit') }}" class="btn btn-warning btn-sm"><i class="fas fa-edit"></i>
                            Edit</a>
                    @else
                        @permission('users.edit')
                        <a href="{{ route('user.edit', ['userToEdit' => $userToShow->id]) }}"
                           class="btn btn-sm btn-warning mr-2 mb-2 mb-md-0"><i class="fas fa-edit"></i> Edit</a>

                        @if(!empty($userToShow->api_key))
                            <form method="POST" action="{{ route('user.apikey.clear', ['user' => $userToShow->id]) }}"
                                  onsubmit="return confirm('This will invalidate the user\'s API Key. Proceed?')">

                                <button type="submit" class="btn btn-sm btn-primary mr-3 mb-3"><i
                                            class="fas fa-code"></i> Clear API Key
                                </button>

                                {{ csrf_field() }}
                            </form>
                        @endif
                        @endpermission

                        @permission('users.delete')
                        <form method="POST" action="{{ route('user.destroy', ['user' => $userToShow->id]) }}"
                              onsubmit="return confirm('Do you really want to delete this user?')">

                            <button type="submit" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i> Delete
                            </button>

                            {{ csrf_field() }}
                            {{ method_field("DELETE") }}
                        </form>
                        @endpermission
                    @endif
                </div>
            </div>
        </div>

    </div>
    <div class="col">
        <div class="card mb-3">
            <div class="card-header">
                <h5 class="mb-0">Access Information</h5>
            </div>
            <div class="card-body">
                <p><strong>Last login</strong>: {{ $userToShow->last_login }}</p>

                <p>
                    <strong>Role(s) granted</strong>:
                @if(count($userToShow->roles) > 0)
                    <ul>
                        @foreach ($userToShow->roles as $thisRole)
                            <li>
                                <a href="{{ route('role.show', ['role' =>  $thisRole->id]) }}">{{ $thisRole->display_name }}</a>
                            </li>
                        @endforeach
                    </ul>
                @else
                    None
                    @endif
                    </p>
            </div>
        </div>
        <div class="card">
            <div class="card-header"><h5 class="mb-0">API Tokens</h5></div>
            <div class="card-body">
                @if ($userToShow->tokens->isNotEmpty())
                    <ul>
                        @foreach ($userToShow->tokens as $token)
                            <li>{{ $token->name }} (expires: {{ is_null($token->expires_at) ? 'never' : $token->expires_at }})</li>
                        @endforeach
                    </ul>
                @else
                    <p>No tokens to show</p>
                @endif
                <a href="{{ route('account.tokens.index', ['show' => 'all']) }}" class="btn btn-sm btn-info">
                    <i class="fas fa-code"></i> Manage Tokens
                </a>
            </div>
        </div>
    </div>

</div>