<a href="{{ $url }}">
    <div class="w-{{ $span ?? '100' }} p-3">
        <div class="card">
            <div class="card-body text-center">
                <i class="{{ $fa }} fa-5x mb-3"></i><br>
                {{ $title }}
            </div>
        </div>
    </div>
</a>