@foreach($items as $item)
    <li @lm_attrs($item) class="m-{{ $margin ?? '2' }} @if($item->hasChildren()) dropdown @endif " @lm_endattrs>
        @if($link = $item->link) <a @lm_attrs($link) @if($item->hasChildren()) class="{{ $linkClass ?? 'nav-link' }}
                dropdown-toggle" data-toggle="dropdown" @else class="{{ $linkClass ?? 'nav-link' }}" @endif @lm_endattrs
                                    href="{!! $item->url() !!}">
            {!! $item->title !!}
            @if($item->hasChildren()) <b class="caret"></b> @endif
        </a>
        @else
            {!! $item->title !!}
        @endif
        @if($item->hasChildren())
            <ul class="dropdown-menu">
                @include('components.bs4-menu-items', ['items' => $item->children(), 'linkClass' => 'dropdown-item', 'margin' => '0'])
            </ul>
        @endif
    </li>
    @if($item->divider)
        <li{!! Lavary\Menu\Builder::attributes($item->divider) !!}></li>
    @endif
@endforeach