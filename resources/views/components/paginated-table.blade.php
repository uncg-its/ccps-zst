<div class="row">
    <div class="col-12">
        <p>{{ $collection->total() }} {{ \Str::plural('result', $collection->total()) }} found.</p>
    </div>
</div>

{{ $table }}

{{ $collection->appends($appends ?? '')->links() }}
