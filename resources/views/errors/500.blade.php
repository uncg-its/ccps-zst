@extends('layouts.wrapper', [
    'pageTitle' => '500'
])

@section('content')
    <h2>500 - Internal Server Error</h2>
    <p>{{ $exception->getMessage() }}</p>
@endsection
