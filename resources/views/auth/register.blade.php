@extends('layouts.wrapper', [
    'pageTitle' => "Register"
])

@section('content')
    <div class="row justify-content-center">
        <div class="col-6">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                @foreach($appLoginMethods as $index => $method)
                    <li role="presentation" class="nav-item">
                        <a class="nav-link {{ $index == 0 ? 'active' : '' }}" href="#{{ $method }}" aria-controls="{{ $method }}" role="tab" data-toggle="tab">{{ ucwords($method) }}</a>
                    </li>
                @endforeach
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                @foreach($appLoginMethods as $index => $method)
                    <div role="tabpanel" class="tab-pane fade {{ $index == 0 ? 'show active' : '' }}" id="{{ $method }}">
                        @include('auth.partials.register.' . $method)
                    </div>
                @endforeach
            </div>

        </div>
    </div>
@endsection