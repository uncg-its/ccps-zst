@extends('layouts.wrapper', [
    'pageTitle' => 'Scheduled Webinars | Index'
])

@section('content')
    <h1>Scheduled Webinars</h1>
    <p>Use this page to search for webinars that have been scheduled by a user, which is not currently available in the Zoom Admin panel.</p>
    <hr>
    <div class="card mb-4">
        <div class="card-header">Search</div>
        <div class="card-body">
            {!! Form::open()->method('get')->route('live.scheduled-webinars.index')->fill(collect(request()->all())) !!}
                <div class="form-row">
                    <div class="col-2">
                        {!! Form::text('email', 'Email address') !!}
                    </div>
                    <div class="col d-flex justify-content-start align-items-center mt-3">
                        {!! Form::submit('Submit')->color('success')->size('sm')->attrs(['class' => 'mr-2']) !!}
                        {!! Form::anchor('Reset', route('live.scheduled-webinars.index'))->color('danger')->size('sm') !!}
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
    @isset($webinars)
        @if($webinars->isEmpty())
            <p>This user does not have any upcoming webinars scheduled.</p>
        @else
            @component('components.paginated-table', ['collection' => $webinars])
                @slot('table')
                    @component('components.table')
                        @slot('th')
                            <th>UUID</th>
                            <th>Webinar ID</th>
                            <th>Type</th>
                            <th>Start Time</th>
                            <th>Topic</th>
                            <th>Actions</th>
                        @endslot
                        @slot('tbody')
                            @foreach ($webinars as $webinar)
                                <tr>
                                    <td>{{ $webinar->uuid }}</td>
                                    <td>{{ $webinar->id }}</td>
                                    <td>{{ \Webinar::$type[$webinar->type] }}</td>
                                    <td>{{ $webinar->start_time ?? 'n/a' }}</td>
                                    <td>{{ $webinar->topic }}</td>
                                    <td>
                                        <a href="{{ route('live.scheduled-webinars.show', $webinar->id) }}" class="btn btn-sm btn-info">
                                            <i class="fas fa-list"></i> Details
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        @endslot
                    @endcomponent
                @endslot
            @endcomponent
        @endif
    @else
        <p>Enter an email address above to search.</p>
    @endisset
@endsection
