@extends('layouts.wrapper', [
    'pageTitle' => 'Live API | Scheduled Webinar Details'
])

@section('content')
    <div class="row">
        <div class="col-6">
            <h1>Webinar ID: {{ $webinar['id'] }}</h1>
        </div>
        <div class="col d-flex justify-content-end align-items-center">
            <a href="{{ \URL::previous() }}" class="btn btn-sm btn-warning">
                <i class="fas fa-arrow-left"></i> Back
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <div class="card mb-3">
                <div class="card-header">Webinar Information</div>
                <div class="card-body">
                    <p><strong>UUID</strong>: {{ $webinar['uuid'] }}</p>
                    <p><strong>Host ID</strong>: {{ $webinar['host_id'] }}</p>
                    <p><strong>Topic</strong>: {{ $webinar['topic'] }}</p>
                    <p><strong>Type</strong>: {{ $webinar['type'] }}</p>
                    <p><strong>Start Time</strong>: {{ $webinar['start_time'] ?? 'not set' }}</p>
                    <p><strong>Duration (min)</strong>: {{ $webinar['duration'] ?? 'not set' }}</p>
                    <p><strong>Time Zome</strong>: {{ $webinar['timezone'] }}</p>
                    <p><strong>Created At</strong>: {{ $webinar['created_at'] }}</p>
                    <p><strong>Contact Name</strong>: {{ $webinar['settings']->contact_name ?? 'none' }}</p>
                    <p><strong>Contact Email</strong>: {{ $webinar['settings']->contact_email ?? 'none' }}</p>
                    <p><strong>Alternative Hosts</strong>: {{ $webinar['settings']->alternative_hosts ?? 'none'}}</p>
                </div>
            </div>
            <div class="card">
                <div class="card-header">Links and Joining Info</div>
                <div class="card-body">
                    <p><strong>Start URL</strong>: {{ $webinar['start_url'] }}</p>
                    <p><strong>Join URL</strong>: {{ $webinar['join_url'] }}</p>
                    <p><strong>Password</strong>: {{ $webinar['password'] ?? 'not set'}}</p>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="card">
                <div class="card-header">Settings</div>
                <div class="card-body">
                    <p><strong>Host Video</strong>: {{ $webinar['settings']->host_video ? 'yes' : 'no' }}</p>
                    <p><strong>Panelists Video</strong>: {{ $webinar['settings']->panelists_video ? 'yes' : 'no' }}</p>
                    <p><strong>Registration Type</strong>: {{ $webinar['settings']->registration_type }}</p>
                    <p><strong>Approval Type</strong>: {{ $webinar['settings']->approval_type }}</p>
                    <p><strong>Audio</strong>: {{ $webinar['settings']->audio }}</p>
                    <p><strong>Auto-recording</strong>: {{ $webinar['settings']->auto_recording }}</p>
                    <p><strong>Registrants Confirmation Email</strong>: {{ $webinar['settings']->registrants_confirmation_email ? 'yes' : 'no' }}</p>
                    <p><strong>Registrants Restrict Number</strong>: {{ $webinar['settings']->registrants_restrict_number }}</p>
                    <p><strong>Post-webinar Survey</strong>: {{ $webinar['settings']->post_webinar_survey ? 'yes' : 'no' }}</p>
                    <p><strong>Close Registration</strong>: {{ $webinar['settings']->close_registration ? 'yes' : 'no' }}</p>
                    <p><strong>Show Share Button</strong>: {{ $webinar['settings']->show_share_button ? 'yes' : 'no' }}</p>
                    <p><strong>Allow Multiple Devices</strong>: {{ $webinar['settings']->allow_multiple_devices ? 'yes' : 'no' }}</p>
                    <p><strong>Practice Session</strong>: {{ $webinar['settings']->practice_session ? 'yes' : 'no' }}</p>
                    <p><strong>HD Video</strong>: {{ $webinar['settings']->hd_video ? 'yes' : 'no' }}</p>
                    <p><strong>QA</strong>: {{ $webinar['settings']->question_answer ? 'yes' : 'no' }}</p>
                </div>
            </div>

        </div>
    </div>
@endsection
