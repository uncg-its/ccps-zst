@extends('layouts.wrapper', [
    'pageTitle' => 'Live API: Users | Index'
])

@section('head')
    @livewireStyles
@append

@section('content')
    @livewire('user-lookup')
@endsection

@section('scripts')
    @livewireScripts
@append
