@extends('layouts.wrapper', [
    'pageTitle' => 'Live API | Index'
])

@section('content')
    <h1>Live API</h1>
    <div class="row">
        @include('components.panel-nav', [
            'url' => route('live.scheduled-meetings.index'),
            'fa' => 'fas fa-hourglass',
            'title' => 'Scheduled Meetings'
        ])
         @include('components.panel-nav', [
            'url' => route('live.scheduled-webinars.index'),
            'fa' => 'fas fa-hourglass',
            'title' => 'Scheduled Webinars'
        ])
        @include('components.panel-nav', [
            'url' => route('live.users.index'),
            'fa' => 'fas fa-users',
            'title' => 'User Search'
        ])
        @include('components.panel-nav', [
            'url' => route('live.meetings.index'),
            'fa' => 'fas fa-calendar',
            'title' => 'Meeting Search'
        ])
    </div>
@endsection
