@extends('layouts.wrapper', [
    'pageTitle' => 'Live API | Scheduled Meeting Details'
])

@section('content')
    <div class="row">
        <div class="col-6">
            <h1>Meeting ID: {{ $meeting['id'] }}</h1>
        </div>
        <div class="col d-flex justify-content-end align-items-center">
            <a href="{{ \URL::previous() }}" class="btn btn-sm btn-warning">
                <i class="fas fa-arrow-left"></i> Back
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <div class="card mb-3">
                <div class="card-header">Meeting Information</div>
                <div class="card-body">
                    <p><strong>UUID</strong>: {{ $meeting['uuid'] }}</p>
                    <p><strong>Host ID</strong>: {{ $meeting['host_id'] }}</p>
                    <p><strong>Topic</strong>: {{ $meeting['topic'] }}</p>
                    <p><strong>Type</strong>: {{ $meeting['type'] }}</p>
                    <p><strong>Start Time</strong>: {{ $meeting['start_time'] ?? 'not set' }}</p>
                    <p><strong>Duration (min)</strong>: {{ $meeting['duration'] ?? 'not set'}}</p>
                    <p><strong>Time Zome</strong>: {{ $meeting['timezone'] }}</p>
                    <p><strong>Created At</strong>: {{ $meeting['created_at'] }}</p>
                    <p><strong>Alternative Hosts</strong>: {{ $meeting['settings']->alternative_hosts ?? 'none' }}</p>

                </div>
            </div>
            <div class="card">
                <div class="card-header">Links and Joining Info</div>
                <div class="card-body">
                    <p><strong>Start URL</strong>: {{ $meeting['start_url'] }}</p>
                    <p><strong>Join URL</strong>: {{ $meeting['join_url'] }}</p>
                    <p><strong>Password</strong>: {{ $meeting['password'] ?? 'not set'}}</p>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="card">
                <div class="card-header">Settings</div>
                <div class="card-body">
                    <p><strong>Host Video</strong>: {{ $meeting['settings']->host_video ? 'yes' : 'no' }}</p>
                    <p><strong>Participant Video</strong>: {{ $meeting['settings']->participant_video ? 'yes' : 'no' }}</p>
                    <p><strong>Join Before Host</strong>: {{ $meeting['settings']->join_before_host ? 'yes' : 'no' }}</p>
                    <p><strong>Mute Upon Entry</strong>: {{ $meeting['settings']->mute_upon_entry ? 'yes' : 'no' }}</p>
                    <p><strong>Registration Type</strong>: {{ $meeting['settings']->registration_type }}</p>
                    <p><strong>Approval Type</strong>: {{ $meeting['settings']->approval_type }}</p>
                    <p><strong>Audio</strong>: {{ $meeting['settings']->audio }}</p>
                    <p><strong>Auto-recording</strong>: {{ $meeting['settings']->auto_recording }}</p>
                    <p><strong>Meeting Authentication</strong>: {{ $meeting['settings']->meeting_authentication ? 'yes' : 'no' }}</p>
                    <p><strong>Authentication Option</strong>: {{ $meeting['settings']->authentication_option ?? 'none' }}</p>
                    <p><strong>Authentication Domains</strong>: {{ $meeting['settings']->authentication_domains ?? 'none' }}</p>
                    <p><strong>Authentication Name</strong>: {{ $meeting['settings']->authentication_name ?? 'none' }}</p>
                    <p><strong>Waiting Romo</strong>: {{ $meeting['settings']->waiting_room ? 'yes' : 'no' }}</p>
                </div>
            </div>

        </div>
    </div>
@endsection
