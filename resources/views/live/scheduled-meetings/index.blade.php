@extends('layouts.wrapper', [
    'pageTitle' => 'Scheduled Meetings | Index'
])

@section('content')
    <h1>Scheduled Meetings</h1>
    <p>Use this page to search for meetings that have been scheduled by a user, which is not currently available in the Zoom Admin panel.</p>
    <hr>
    <div class="card mb-4">
        <div class="card-header">Search</div>
        <div class="card-body">
            {!! Form::open()->method('get')->route('live.scheduled-meetings.index')->fill(collect(request()->all())) !!}
                <div class="form-row">
                    <div class="col-2">
                        {!! Form::text('email', 'Email address') !!}
                    </div>
                    <div class="col d-flex justify-content-start align-items-center mt-3">
                        {!! Form::submit('Submit')->color('success')->size('sm')->attrs(['class' => 'mr-2']) !!}
                        {!! Form::anchor('Reset', route('live.scheduled-meetings.index'))->color('danger')->size('sm') !!}
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
    @isset($meetings)
        @if($meetings->isEmpty())
            <p>This user does not have any upcoming meetings scheduled.</p>
        @else
            @component('components.paginated-table', ['collection' => $meetings])
                @slot('table')
                    @component('components.table')
                        @slot('th')
                            <th>UUID</th>
                            <th>Meeting ID</th>
                            <th>Type</th>
                            <th>Start Time</th>
                            <th>Topic</th>
                            <th>Actions</th>
                        @endslot
                        @slot('tbody')
                            @foreach ($meetings as $meeting)
                                <tr>
                                    <td>{{ $meeting->uuid }}</td>
                                    <td>{{ $meeting->id }}</td>
                                    <td>{{ \Meeting::$type[$meeting->type] }}</td>
                                    <td>{{ $meeting->start_time ?? 'n/a' }}</td>
                                    <td>{{ $meeting->topic }}</td>
                                    <td>
                                        <a href="{{ route('live.scheduled-meetings.show', $meeting->id) }}" class="btn btn-sm btn-info">
                                            <i class="fas fa-list"></i> Details
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        @endslot
                    @endcomponent
                @endslot
            @endcomponent
        @endif
    @else
        <p>Enter an email address above to search.</p>
    @endisset
@endsection
