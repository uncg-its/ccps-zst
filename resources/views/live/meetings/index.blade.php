@extends('layouts.wrapper', [
    'pageTitle' => 'Live API: Meetings | Index'
])

@section('head')
    @livewireStyles
@append

@section('content')
    @livewire('meeting-search')
@endsection

@section('scripts')
    @livewireScripts
@append
