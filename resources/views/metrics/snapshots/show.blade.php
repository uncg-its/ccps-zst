@extends('layouts.wrapper', [
    'pageTitle' => 'Metrics Snapshot ' . $metricsSnapshot->id
])

@section('content')
    <div class="row">
        <div class="col-6">
            <h1>Metrics Snapshot {{ $metricsSnapshot->id }}</h1>
        </div>
        <div class="col-6 d-flex justify-content-end align-items-center">
            <a href="{{ route('metrics.snapshots.index') }}" class="btn btn-sm btn-warning">
                <i class="fa fa-arrow-left"></i> Back to list
            </a>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-6">
            <div class="card">
                <div class="card-header">General Information</div>
                <div class="card-body">
                    <p><strong>Date</strong>: {{ $metricsSnapshot->date }}</p>
                    <p><strong>Created At</strong>: {{ $metricsSnapshot->created_at }}</p>
                    <p><strong>Pushed At</strong>: {{ $metricsSnapshot->pushed_at }}</p>
                    <p><strong>Push Destination(s)</strong>: {{ implode(', ', $metricsSnapshot->push_destination ?? []) }}</p>
                    <p><strong>Error Data</strong>: {{ $metricsSnapshot->error_data }}</p>
                </div>
            </div>
            <div class="card mt-3">
                <div class="card-header">Actions</div>
                <div class="card-body d-flex">
                    @permission('metrics.view')
                        @if (is_null($metricsSnapshot->pushed_at))
                            <div>
                                {!! Form::open()->route('metrics.snapshots.push', ['metrics_snapshot' => $metricsSnapshot])->method('post') !!}
                                {!! Form::submit('<i class="fas fa-upload"></i> Push Now')->size('sm')->color('success')->attrs(['class' => 'mr-2']) !!}
                                {!! Form::close() !!}
                            </div>
                        @endif
                    @endpermission
                    @permission('metrics.delete')
                        <div>
                            {!! Form::open()->method('delete')->route('metrics.snapshots.destroy', ['metrics_snapshot' => $metricsSnapshot]) !!}
                            {!! Form::submit('<i class="fas fa-trash"></i> Delete')->color('danger')->size('sm')->attrs($metricsSnapshot->pushed ? ['disabled' => ''] : ['onclick' => 'return confirm("Are you sure?");']) !!}
                            {!! Form::close() !!}
                            @if ($metricsSnapshot->pushed)
                                <p class="text-muted mt-3"><small><em>Note: pushed snapshots cannot be deleted</em></small></p>
                            @endif
                        </div>
                    @endpermission
                </div>
            </div>

        </div>
        <div class="col-6">
            <div class="card">
                <div class="card-header">Metrics</div>
                <div class="card-body">
                    <pre>{{ json_encode($metricsSnapshot->raw_data, JSON_PRETTY_PRINT) }}</pre>
                </div>
            </div>

        </div>
    </div>
@endsection()
