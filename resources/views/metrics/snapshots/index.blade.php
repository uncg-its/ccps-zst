@extends('layouts.wrapper', [
    'pageTitle' => 'Metrics Snapshots | Index'
])

@section('head')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/zebra_datepicker@1.9.12/dist/css/bootstrap/zebra_datepicker.min.css">
@append

@section('content')
    <div class="row">
        <div class="col-6">
            <h1>Metrics Snapshots</h1>
        </div>
        <div class="col-6 d-flex justify-content-end align-items-center">
            <a href="{{ route('metrics.snapshots.create') }}" class="btn btn-sm btn-success">
                <i class="fa fa-plus"></i> Create New
            </a>
        </div>
    </div>
    <p>
        <strong>Metrics Snapshots</strong> attempt to collect useful metrics for the Zoom service. These snapshots are broken down into daily usage.
    </p>

    <p>Browse generated metrics snapshots below. If you need to do so, you may generate any missing snapshots by using the "Create New" button above.</p>
    <hr>

    <div class="row">
        <div class="col">
            @if (request()->anyFilled(array_keys($repository->searchable)))
            <div class="alert alert-info">
                Note: filtering on
            </div>
            @endif

            <div class="card mb-4">
                <div class="card-header">Search / Filter</div>
                <div class="card-body">
                    {!! Form::open()->method('get')->route('metrics.snapshots.index')->fill(collect(request()->all())) !!}
                        <div class="form-row">
                            <div class="col-2">
                                {!! Form::text('target_date_start', 'Target Date (start)') !!}
                            </div>
                            <div class="col-2">
                                {!! Form::text('target_date_end', 'Target Date (end)') !!}
                            </div>
                            <div class="col d-flex justify-content-start align-items-center mt-3">
                                {!! Form::submit('Submit')->color('success')->size('sm')->attrs(['class' => 'mr-2']) !!}
                                {!! Form::anchor('Reset', route('metrics.snapshots.index'))->color('danger')->size('sm') !!}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>

            @if ($metricsSnapshots->isNotEmpty())
                @component('components.paginated-table', ['collection' => $metricsSnapshots, 'appends' => request()->only(array_keys($repository->searchable))])
                    @slot('table')
                        @component('components.table')
                            @slot('th')
                                <th>ID</th>
                                <th>Date</th>
                                <th>Created At</th>
                                @role('admin')
                                    <th>Trashed At</th>
                                @endrole
                                <th>Pushed At</th>
                                <th>Destinations</th>
                                <th>Actions</th>
                            @endslot
                            @slot('tbody')
                                @foreach ($metricsSnapshots as $snapshot)
                                    <tr @if($snapshot->trashed()) class="text-muted bg-light" @endif>
                                        <td>
                                            @if($snapshot->trashed())
                                                <i class="fa fa-trash"></i>
                                            @endif
                                            {{ $snapshot->id }}
                                        </td>
                                        <td>{{ $snapshot->date }}</td>
                                        <td>{{ $snapshot->created_at }}</td>
                                        @role('admin')
                                            <td>{{ $snapshot->deleted_at }}</td>
                                        @endrole
                                        <td>{{ $snapshot->pushed_at }}</td>
                                        <td>{{ implode(',', $snapshot->push_destination ?? []) }}</td>
                                        <td>
                                            @permission('metrics.view')
                                                @if (! $snapshot->trashed())
                                                    <a href="{{ route('metrics.snapshots.show', $snapshot) }}" class="btn btn-sm btn-info">
                                                        <i class="fa fa-list"></i> Details
                                                    </a>
                                                @else
                                                    {!! Form::open()->route('metrics.snapshots.restore', ['metrics_snapshot' => $snapshot])->method('post') !!}
                                                    {!! Form::submit('<i class="fa fa-undo"></i> Restore')->size('sm')->color('outline-danger') !!}
                                                    {!! Form::close() !!}
                                                @endif
                                            @endpermission
                                        </td>
                                    </tr>
                                @endforeach
                            @endslot
                        @endcomponent
                    @endslot
                @endcomponent
            @else
                <p>No Metrics Snapshots exist.</p>
            @endif
        </div>
    </div>
@endsection()

@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/zebra_datepicker@1.9.12/dist/zebra_datepicker.min.js"></script>
    <script>
        $(function() {
            $('#target_date_start, #target_date_end').Zebra_DatePicker({
                format: 'Y-m-d',
            });

            $('.invalid-feedback').show();
        });
    </script>
@append
