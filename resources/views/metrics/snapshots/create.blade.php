@extends('layouts.wrapper', [
    'pageTitle' => 'Metrics Snapshots | Create'
])

@section('head')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/zebra_datepicker@1.9.12/dist/css/bootstrap/zebra_datepicker.min.css">
@append

@section('content')
    <h1>Create Metrics Snapshot</h1>
    <p>Metrics Snapshots are generally created automatically, on a daily basis. However, this form is useful if you know that you need to generate a snapshot for today.</p>
    <hr>
    {!! Form::open()->route('metrics.snapshots.store')->method('post')->fill(collect(request()->old())) !!}
    <div class="form-row">
        <div class="col">
            {!! Form::text('date', 'Date')->required()->help('The date for which to create the snapshot') !!}
        </div>
    </div>
    <div class="form-row mt-3">
        <div class="col">
            {!! Form::submit('<i class="fas fa-check"></i> Submit')->color('success') !!}
        </div>
    </div>
    {!! Form::close() !!}
@endsection()

@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/zebra_datepicker@1.9.12/dist/zebra_datepicker.min.js"></script>
    <script>
        $(function() {
            $('#date').Zebra_DatePicker({
                format: 'Y-m-d',
                direction: false,
            });

            $('.invalid-feedback').show();
        });
    </script>
@append
