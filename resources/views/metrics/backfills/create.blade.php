@extends('layouts.wrapper', [
    'pageTitle' => 'Metrics Backfills | Create'
])

@section('head')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/zebra_datepicker@1.9.12/dist/css/bootstrap/zebra_datepicker.min.css">
@append

@section('content')
    <h1>Metrics Backfills - Create</h1>
    <p>Use this form to create a new metrics backfill that spans the dates you need filled.</p>
    <div class="card">
        <div class="card-header">New Backfill</div>
        <div class="card-body">
            {!! Form::open()->method('post')->route('metrics.backfills.store') !!}
            {!! Form::text('from', 'From Date (YYYY-MM-DD)')->required()->help('Required. Must be in YYYY-MM-DD format. Inclusive.') !!}
            {!! Form::text('to', 'To Date (YYYY-MM-DD)')->required()->help('Required. Must be in YYYY-MM-DD format. Inclusive.') !!}
            {!! Form::submit('<i class="fas fa-check"></i> Submit')->color('success') !!}
            {!! Form::close() !!}
        </div>
    </div>

@endsection

@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/zebra_datepicker@1.9.12/dist/zebra_datepicker.min.js"></script>
    <script>
        $(function() {
            $('#from, #to').Zebra_DatePicker({
                format: 'Y-m-d',
            });

            $('.invalid-feedback').show();
        });
    </script>
@append
