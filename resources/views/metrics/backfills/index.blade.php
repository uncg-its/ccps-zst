@extends('layouts.wrapper', [
    'pageTitle' => 'Metrics Backfills | Index'
])

@section('content')
    <h1>Metrics Backfills</h1>
    <p>When gaps are discovered in metrics, this process can be used to fill them.</p>
    @if (\Auth::user()->hasPermission('metrics.create'))
    <p>
        <a href="{{ route('metrics.backfills.create') }}" class="btn btn-success">
            <i class="fas fa-plus"></i> Create new
        </a>
    </p>
    @endif

    <hr>
    @if ($metricsBackfills->isEmpty())
        <p>No metrics backfills have been initiated.</p>
    @else
        @component('components.paginated-table', ['collection' => $metricsBackfills])
            @slot('table')
                @component('components.table')
                    @slot('th')
                        <th>ID</th>
                        <th>Requester</th>
                        <th>From</th>
                        <th>To</th>
                        <th>Status</th>
                    @endslot
                    @slot('tbody')
                        @foreach ($metricsBackfills as $backfill)
                            <tr>
                                <td>{{ $backfill->id }}</td>
                                <td>{{ $backfill->requester->email }}</td>
                                <td>{{ $backfill->from }}</td>
                                <td>{{ $backfill->to }}</td>
                                <td>{{ $backfill->status }}</td>
                            </tr>
                        @endforeach
                    @endslot
                @endcomponent
            @endslot
        @endcomponent
    @endif
@endsection
