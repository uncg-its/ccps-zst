@extends('layouts.wrapper', [
    'pageTitle' => 'Metrics | Index'
])

@section('content')
    <h1>Metrics</h1>
    <div class="row">
        @permission('metrics.*')
            @include('components.panel-nav', [
                    'url' => route('metrics.snapshots.index'),
                    'fa' => 'fas fa-camera',
                    'title' => 'Metrics Snapshots'
            ])
            @include('components.panel-nav', [
                'url' => route('metrics.backfills.index'),
                'fa' => 'fas fa-wrench',
                'title' => 'Metrics Backfills'
            ])
        @endpermission
    </div>
@endsection
