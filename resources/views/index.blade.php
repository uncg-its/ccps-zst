@extends('layouts.wrapper', [
    'pageTitle' => 'Home'
])

@section('content')
    <h1>{{ config('app.name') }}</h1>
    <div class="row">
        @if($user)

        @permission('api.view')
            @include('components.panel-nav', [
                'url' => route('live.index'),
                'fa' => 'fas fa-laptop-code',
                'title' => 'Live API'
            ])
        @endpermission
        {{-- @permission('metrics.*')
            @include('components.panel-nav', [
                'url' => route('metrics.index'),
                'fa' => 'fas fa-chart-line',
                'title' => 'Metrics'
            ])
        @endpermission --}}

        @foreach($modules as $key => $module)
            @if(empty($module['parent']))
                @permission($module['required_permissions'])
                @include('components.panel-nav', [
                    'url' => route($module['index']),
                    'fa' => $module['icon'],
                    'title' => $module['title']
                ])
                @endpermission
            @endif
        @endforeach

        @role('admin')
            @include('components.panel-nav', [
                'url' => route('admin'),
                'fa' => 'fas fa-key',
                'title' => 'Administrator'
            ])
        @endrole

        @else
            <div class="col-md-12">
                <p>You are not logged in - please <a href="{{ route('login') }}">log in</a> to proceed.</p>
            </div>
        @endif
    </div>
@endsection
