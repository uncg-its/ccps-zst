@extends('layouts.wrapper', [
    'pageTitle' => 'Notifications | Index'
])

@section('content')
    {!! Breadcrumbs::render('notifications.index') !!}

    <h1>My Notification Settings</h1>

    <div class="row">
        <div class="col-6">
            <h2>Notification Channels</h2>
        </div>
        <div class="col-6 d-flex justify-content-end align-items-center">
            <a href="{{ route('account.notifications.channels.create') }}" class="btn btn-sm btn-success mb-3">
                <i class="fas fa-plus"></i> Add New
            </a>
        </div>
    </div>

    @if($notificationChannels->isNotEmpty())
        @component('components.table')
            @slot('th')
                <th>Name</th>
                <th>Type</th>
                <th>Value</th>
                <th>Actions</th>
            @endslot
            @slot('tbody')
                @foreach($notificationChannels as $channel)
                    <tr>
                        <td>
                            {{ $channel->name }}
                            @if(!$channel->verified)
                                <span class="text-danger" data-toggle="tooltip" data-placement="top" title="Unverified"><i
                                            class="fas fa-exclamation-circle"></i></span>
                            @endif
                        </td>
                        <td>
                            {{ $channel->type }} @if($channel->type == 'sms') - {{ $channel->carrier }} @endif
                        </td>
                        <td>{{ $channel->key_formatted }}</td>
                        <td class="d-flex justify-content-start">
                            @if(!$channel->verified)
                                @if($channel->channel_verifications->isNotEmpty())
                                    <a href="{{ route('account.notifications.channels.verify', ['channel' => $channel]) }}"
                                       class="btn btn-success btn-sm mr-2"><i class="fas fa-shield-alt"></i> Verify</a>
                                @endif

                                <a href="{{ route('account.notifications.channels.resend-verification', ['channel' => $channel]) }}"
                                   class="btn btn-info btn-sm mr-2"><i class="fas fa-redo"></i> Resend
                                    verification</a>
                            @endif
                            @if($channel->verified)
                                <a href="{{ route('account.notifications.channels.test', ['channel' => $channel]) }}"
                                   class="btn btn-sm btn-warning mr-2"><i class="fas fa-vial"></i> Send Test</a>
                            @endif
                            {!! Form::open()->method('delete')->route('account.notifications.channels.destroy', ['channel' => $channel]) !!}
                            <button type="submit" class="btn btn-sm btn-danger"
                                    onclick="return(confirm('Are you sure you want to delete this channel?'))"><i
                                        class="fas fa-trash"></i> Delete
                            </button>
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
            @endslot
        @endcomponent
    @else
        <p>No notification channels configured.</p>
    @endif

    <hr>

    <div class="row">
        <div class="col-6">
            <h2>Notification Settings</h2>
        </div>
        <div class="col-6 d-flex justify-content-end align-items-center">
            @if ($verifiedNotificationChannels->isNotEmpty() && $notificationEvents->isNotEmpty())
                <a href="{{ route('account.notifications.configure') }}" class="btn btn-sm btn-warning mb-3">
                    <i class="fas fa-cogs"></i> Configure
                </a>
            @endif
        </div>
    </div>

    @if($verifiedNotificationChannels->isNotEmpty() && $notificationEvents->isNotEmpty())
        @component('components.table')
            @slot('th')
                <th></th>
                @foreach($verifiedNotificationChannels as $channel)
                    <th>{{ $channel->name }}</th>
                @endforeach
            @endslot
            @slot('tbody')
                @foreach($notificationEvents as $event)
                    <tr>
                        <th scope="row" data-toggle="tooltip" data-placement="top"
                            title="{{ $event->description }}">{{ $event->display_name }}</th>
                        @foreach ($verifiedNotificationChannels as $channel)
                            <td>
                                @if($channel->notification_events->where('id', $event->id)->isNotEmpty())
                                    <span class="text-success"><i class="fas fa-check"></i> Enabled</span>
                                @else
                                    <span class="text-danger"><i class="fas fa-times"></i> Disabled</span>
                                @endif
                            </td>
                        @endforeach
                    </tr>
                @endforeach
            @endslot
        @endcomponent
    @else
        @if ($verifiedNotificationChannels->isEmpty())
            <p>No notification channels have been verified.</p>
        @else
            <p>No notifiable events have been configured for this application.</p>
        @endif
    @endif
@endsection

@section('scripts')
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection
