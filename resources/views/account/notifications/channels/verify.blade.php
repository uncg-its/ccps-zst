@extends('layouts.wrapper', [
    'pageTitle' => 'Notification Channel | Verify'
])

@section('content')
    <h1>Verify Notification Channel</h1>

    <div class="row">
        <div class="col-6 offset-3">
            <div class="card">
                <div class="card-header">
                    <h5 class="mb-0">Enter code</h5>
                </div>
                <div class="card-body">
                    <p>Please enter the verification code that you received for your channel:
                        <strong>{{ $channel->name }}</strong></p>
                    {!! Form::open()->method('get')->route('account.notifications.channels.verify', ['channel' => $channel]) !!}
                    {!! Form::text('code', 'Code') !!}
                    {!! Form::submit('<i class="fas fa-shield-alt"></i> Verify') !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
