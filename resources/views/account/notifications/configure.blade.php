@extends('layouts.wrapper', [
    'pageTitle' => 'Notifications | Configure'
])

@section('content')
    {!! Breadcrumbs::render('notifications.configure') !!}
    <h2>Configure Notifications</h2>
    <p>Choose the channels at which you wish to receive notifications for each given event.</p>
    @if($notificationChannels->isNotEmpty())
        {!! Form::open()->method('post')->route('account.notifications.configure.save') !!}
        @component('components.table')
            @slot('th')
                <th></th>
                @foreach($notificationChannels as $channel)
                    @if($channel->verified)
                        <th>{{ $channel->name }}</th>
                    @endif
                @endforeach
            @endslot
            @slot('tbody')
                @foreach($notificationEvents as $event)
                    <tr>
                        <th scope="row" data-toggle="tooltip" data-placement="top"
                            title="{{ $event->description }}">{{ $event->display_name }}</th>
                        @foreach ($notificationChannels as $channel)
                            <td>
                               <input type="checkbox" name="channel_event[]"
                                  value="{{ $channel->id }}_{{ $event->id }}"
                                  @if($channel->notification_events->where('id', $event->id)->isNotEmpty()) checked @endif>
                            </td>
                        @endforeach
                    </tr>
                @endforeach
            @endslot
        @endcomponent
        {!! Form::submit('<i class="fas fa-save"></i> Save')->color('success') !!}
        {!! Form::close() !!}
    @else
        <p>No notifications configured yet.</p>
    @endif
@endsection
