@extends('layouts.wrapper', [
    'pageTitle' => 'My Account'
])

@section('content')
    {!! Breadcrumbs::render('account') !!}
    <div class="row">
        <div class="col mx-auto">
            <h1>My Account</h1>
        </div>
        <div class="col mx-auto">
            <p class="text-right mt-3"><a class="btn btn-sm btn-danger" href="{{ route('logout') }}"
                  onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class="fas fa-power-off"></i> Log Out
                </a></p>
        </div>
    </div>

    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>

    <div class="row">
        @include('components.panel-nav', [
            'url' => route('profile.show'),
            'fa' => 'fas fa-user',
            'title' => 'My Profile'
        ])
        @include('components.panel-nav', [
            'url' => route('account.settings'),
            'fa' => 'fas fa-cog',
            'title' => 'Account Settings'
        ])
        @include('components.panel-nav', [
            'url' => route('account.notifications.index'),
            'fa' => 'fas fa-bell',
            'title' => 'Notifications'
        ])
        @permission('tokens.*')
            @include('components.panel-nav', [
                'url' => route('account.tokens.index'),
                'fa' => 'fas fa-code',
                'title' => 'API Keys'
            ])
        @endpermission
    </div>
@endsection
