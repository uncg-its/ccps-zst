@extends('layouts.wrapper', [
    'pageTitle' => 'My Profile'
])

@section('content')
    {!! Breadcrumbs::render('profile.show') !!}

    @include('partials.user-show', [
        'userToShow' => $user,
        'context' => 'profile'
    ])


@endsection
