@extends('layouts.wrapper', [
    'pageTitle' => 'API Tokens | Edit'
])

@section('content')
    {!! Breadcrumbs::render('tokens.edit', $token) !!}
    <h1>Edit API Token</h1>
    <hr>
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">Token Information</div>
                <div class="card-body">
                    {!! Form::open()->patch()->route('account.tokens.update', ['token' => $token]) !!}
                    {!! Form::text('name', 'Name', old('token', $token->name))->required()->help('A name for the token, so that you can tell it apart from others you have created. Typically this will refer in some way to how the token will be used.') !!}
                    {!! Form::submit('<i class="fas fa-check"></i> Submit')->color('success') !!}
                    {!! Form::close() !!}
                </div>
            </div>

        </div>
    </div>
@endsection
