@extends('layouts.wrapper', [
    'pageTitle' => 'Account | API Tokens'
])

@section('content')
    {!! Breadcrumbs::render('tokens.index') !!}

    <div class="row">
        <div class="col">
            <h1>API Tokens</h1>
        </div>
        <div class="col d-flex justify-content-end align-items-center">
            @permission('tokens.create')
                <a href="{{ route('account.tokens.create') }}" class="btn btn-sm btn-success mr-2">
                    <i class="fas fa-plus"></i> Create New
                </a>
            @endpermission
            @permission('tokens.admin')
            @if ($showAll)
                <a href="{{ route('account.tokens.index') }}" class="btn btn-sm btn-info">
                    <i class="fas fa-user"></i> My Tokens
                </a>
            @else
                <a href="{{ route('account.tokens.index', ['show' => 'all']) }}" class="btn btn-sm btn-info">
                    <i class="fas fa-list"></i> Show All
                </a>
            @endif
            @endpermission
        </div>
    </div>
    <p>Generate and manage your API tokens on this page.</p>
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#tokenInstructions">
        <i class="fas fa-question-circle"></i>
        Usage Instructions
    </button>
    <!-- Modal -->
    <div class="modal fade" id="tokenInstructions" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="tokenInstructionsLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="tokenInstructionsLabel">API Token instructions</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Requests to this application should be made as described in the application's API documentation, authenticating via a Bearer token. This header should be present on all requests that require authentication:</p>
                <div class="bg-light p-3">
                    <pre class="m-0">Authorization: Bearer API_TOKEN_VALUE_HERE</pre>
                </div>
            </div>
            </div>
        </div>
    </div>
    <hr>
    @if ($showAll)
        <div class="alert alert-info">
            Showing tokens for all users
        </div>
    @endif
    @if ($tokens->isEmpty())
        <p>No API Tokens have been created.</p>
    @else
        @component('components.paginated-table', ['collection' => $tokens])
            @slot('table')
                @component('components.table')
                    @slot('th')
                        @permission('tokens.admin')
                            @if ($showAll)
                                <th>User</th>
                            @endif
                        @endpermission
                        <th>Name</th>
                        <th>Abilities</th>
                        <th>Last Used</th>
                        <th>Created At</th>
                        <th>Expires At</th>
                        <th>Actions</th>
                    @endslot
                    @slot('tbody')
                        @foreach ($tokens as $token)
                            <tr>
                                @permission('tokens.admin')
                                    @if ($showAll)
                                        <td>{{ $token->tokenable->email }}</td>
                                    @endif
                                @endpermission
                                <td>{{ $token->name }}</td>
                                <td>{{ implode(',', $token->abilities) }}</td>
                                <td>{{ $token->last_used }}</td>
                                <td>{{ $token->created_at }}</td>
                                <td>{{ $token->expires_at }}</td>
                                <td class="d-flex justify-content-start align-items-center">
                                    @permission('tokens.edit')
                                        <a href="{{ route('account.tokens.edit', $token) }}" class="btn btn-warning btn-sm mr-2">
                                            <i class="fas fa-edit"></i> Edit
                                        </a>
                                    @endpermission
                                    @permission('tokens.revoke')
                                        {!! Form::open()->delete()->route('account.tokens.revoke', ['token' => $token]) !!}
                                        {!! Form::submit('<i class="fas fa-trash"></i> Revoke')->attrs(['class' => 'btn btn-sm btn-danger mr-2', 'onclick' => 'return confirm("Revoking a token is final and cannot be undone. Are you sure you want to continue?")']) !!}
                                        {!! Form::close() !!}
                                    @endpermission
                                </td>
                            </tr>
                        @endforeach
                    @endslot
                @endcomponent
            @endslot
        @endcomponent

        @if (session()->has('newToken'))
            <!-- Modal -->
            <div class="modal fade" id="newToken" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="newTokenLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="newTokenLabel">New API Token</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Your new token has been created. <strong>Copy this key</strong> in a safe place, as it will not be shown again.</p>
                    <div class="text-wrap lead">
                        <code>{{ session('newToken')->plainTextToken }}</code>
                    </div>
                    <p class="mt-3"><strong>Note that your token is not granted any abilities by default</strong>. However, this comes into play only if the application has implemented ability checks in the API.</p>
                </div>
                </div>
            </div>
            </div>
        @endif
    @endif
@endsection

@section('scripts')
    <script>
        $(function() {
            $('#newToken').modal('show');
        });
    </script>
@endsection
