@extends('layouts.wrapper', [
    'pageTitle' => 'Account Settings'
])

@section('content')
    {!! Breadcrumbs::render('account.settings', $user) !!}

    <h1>My Account Settings</h1>

    <div class="row">
        <div class="col-md-12">
            <form action="{{ route('account.settings.update') }}" method="post">
                <div class="form-group">
                    <label for="skin">Interface Skin</label>
                    <select class="form-control" id="skin" name="skin">
                        @foreach($skins as $name => $attributes)
                            <option value="{{ $name }}" @if($name == $selected)
                            selected
                                    @endif
                            >{{ $attributes['display_name'] }}</option>
                        @endforeach
                    </select>
                    <small class="form-text form-muted">Change the appearance ("theme") of the application</small>
                </div>

                <button type="submit" class="btn btn-primary">Submit</button>
                {!! csrf_field() !!}
            </form>
        </div>
    </div>

@endsection
