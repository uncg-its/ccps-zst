<div>
    <h1>Meeting Search</h1>
    <p>Use this tool to look up an <strong>upcoming Zoom meeting</strong> by number</p>
    <div class="card mb-3">
        <div class="card-header">Search</div>
        <div class="card-body">
            <div class="form-inline">
                <label for="meetingNumber">Meeting Number</label>
                <input type="text" wire:model.debounce.1000ms="meetingNumber" wire:loading.attr="disabled" class="form-control mx-2" id="meetingNumber">
            </div>
        </div>
    </div>

    <div class="alert alert-info d-none" wire:loading.class="d-block">
        <i class="fas fa-spin fa-spinner"></i> Loading...
    </div>

    @if($result === 'not-found')
    <div class="alert alert-warning" wire:loading.remove>
        <i class="fas fa-exclamation-triangle"></i> Result not found
    </div>
    @endif

    @if($result === 'error')
    <div class="alert alert-danger" wire:loading.remove>
        <i class="fas fa-exclamation-triangle"></i> API error while searching
    </div>
    @endif

    @if($zoomMeeting)
    <div wire:loading.remove>
        <div class="card">
                <div class="card-header">
                    <h5>Meeting information</h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <p><strong>UUID</strong>: {{ $zoomMeeting['uuid'] }}</p>
                            <p><strong>ID</strong>: {{ $zoomMeeting['id'] }}</p>
                            <p><strong>Host Email</strong>: {{ $zoomMeeting['host_email'] }}</p>
                            <p><strong>Topic</strong>: {{ $zoomMeeting['topic'] }}</p>
                            <p><strong>Start Time</strong>: {{ data_get($zoomMeeting, 'start_time', '') }}</p>
                            <p><strong>Join URL</strong>: <a href="{{ $zoomMeeting['join_url'] }}">{{ $zoomMeeting['join_url'] }}</a></p>
                        </div>
                    </div>

                    <h5>Full Data</h5>
                    <div class="bg-light p-4">
                        <pre>{{ json_encode($zoomMeeting, JSON_PRETTY_PRINT) }}</pre>
                    </div>
                </div>
            </div>
    </div>
    @endif
</div>
