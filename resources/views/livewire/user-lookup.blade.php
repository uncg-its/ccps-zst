<div>
    <h1>User Lookup</h1>
    <p>Use this tool to look up existing users in Zoom, and to get their current information from the API.</p>
    <div class="card mb-3">
        <div class="card-header">Search</div>
        <div class="card-body">
            <div class="form-inline">
                <label for="username">Username</label>
                <input type="text" wire:model.debounce.1000ms="username" wire:loading.attr="disabled" class="form-control mx-2" id="username">
            </div>
        </div>
    </div>

    <div class="alert alert-info d-none" wire:loading.class="d-block">
        <i class="fas fa-spin fa-spinner"></i> Loading...
    </div>

    @if($result === 'not-found')
    <div class="alert alert-warning" wire:loading.remove>
        <i class="fas fa-exclamation-triangle"></i> Result not found
    </div>
    @endif

    @if($result === 'error')
    <div class="alert alert-danger" wire:loading.remove>
        <i class="fas fa-exclamation-triangle"></i> API error while searching
    </div>
    @endif

    @if($zoomUser)
    <div wire:loading.remove>
        <div class="card">
                <div class="card-header">
                    <h5>User information</h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <p><strong>ID</strong>: {{ $zoomUser['id'] }}</p>
                            <p><strong>First Name</strong>: {{ $zoomUser['first_name'] }}</p>
                            <p><strong>Last Name</strong>: {{ $zoomUser['last_name'] }}</p>
                            <p><strong>Department</strong>: {{ $zoomUser['dept'] }}</p>
                            <p><strong>Personal Meeting URL</strong>: <a href="{{ $zoomUser['personal_meeting_url'] }}">{{ $zoomUser['personal_meeting_url'] }}</a></p>
                            <p><strong>Created At</strong>: {{ $zoomUser['created_at'] }}</p>
                            <p><strong>Last Login</strong>: {{ $zoomUser['last_login_time'] }}</p>
                            <p><strong>Last Client Version</strong>: {{ $zoomUser['last_client_version'] ?? '' }}</p>
                        </div>
                        <div class="col-2">
                            @if(isset($zoomUser['pic_url']))
                                <img src="{{ $zoomUser['pic_url'] }}" alt="user avatar" class="img-fluid rounded">
                            @else
                                <p class="border p-4 text-center">No avatar</p>
                            @endif
                        </div>
                    </div>

                    <h5>Full Data</h5>
                    <div class="bg-light p-4">
                        <pre>{{ json_encode($zoomUser, JSON_PRETTY_PRINT) }}</pre>
                    </div>
                </div>
            </div>
    </div>
    @endif
</div>
